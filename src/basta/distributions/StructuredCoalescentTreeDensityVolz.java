/*
 * Copyright (C) 2015 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package basta.distributions;


//import multitypetree.operators.UniformizationRetypeOperator.NoValidPathException;
//import beastfx.app.beast.BeastMCMC;

import basta.evolution.tree.MigrationModelVolz;
import basta.evolution.tree.MultiTypeNodeVolz;
import basta.evolution.tree.MultiTypeTreeFromNewickVolz;
import basta.evolution.tree.MultiTypeTreeVolz;
import beast.base.core.Citation;
import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.core.Loggable;
import beast.base.evolution.alignment.Taxon;
import beast.base.evolution.alignment.TaxonSet;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.TraitSet;
import beast.base.evolution.tree.Tree;
import beast.base.inference.parameter.RealParameter;
import beast.base.util.Randomizer;
import com.google.common.collect.Lists;
import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

import java.io.PrintStream;
import java.util.*;

/**
 *
 * @author Nicola De Maio
 */
@Citation("New Routes to Phylogeography. De Maio N., Wu C., O'Reilly K., Wilson D. 2015, PLOS Genetics")
@Description("Likelihood of Coalescent tree (Volz-type) under the Volz approximation to the structured coalescent.")
public class StructuredCoalescentTreeDensityVolz extends MultiTypeTreeDistributionVolz implements Loggable {

    public Input<MigrationModelVolz> migrationModelInput = new Input<MigrationModelVolz>(
            "migrationModelVolz", "Model of migration between demes.", Validate.REQUIRED);
    public Input<Boolean> checkValidityInput = new Input<Boolean>(
            "checkValidity", "Explicitly check validity of colouring (Default false).  "
            +"Useful if operators are in danger of proposing invalid trees.",
            false);
    
    protected MigrationModelVolz migrationModelVolz;
    //protected MigrationModelVolz storedMigrationModelVolz;
    protected MultiTypeTreeVolz mtTree;
    protected boolean checkValidity;
    public boolean sampleMig=false;
    public boolean loggable=true;
    public Double[][] pLikes = null;
    public Double[][] storedpLikes = null;
    protected enum SCEventKind {
        COALESCE, SAMPLE
    };

    protected class SCEvent {
        double time;
        int type;
        SCEventKind kind;
        Node node;
    }
    protected List<SCEvent> eventList;
    protected List<Double[]> lineageCountList;
    protected List<Double[]> lineageCountSquareList;
    protected List<Double[]> lineageProbsList;
    protected List<Double[]> lineageCountListAfter;
    protected List<Double[]> lineageCountSquareListAfter;
    public int[] migrationCounts;
    public int[] migrationCountsStored;
    public int[] nodeTypes;
    public int[] nodeTypesStored;
    public int nDemes;
    
    @Override
    public void store() {
    	storedpLikes=(Double[][])pLikes.clone();
        storedLogP = logP;
        migrationCountsStored=migrationCounts.clone();
        nodeTypesStored=nodeTypes.clone();
        //migrationModelVolz.store();
        //storedMigrationModelVolz=migrationModelVolz.;
        super.store();
    }

    @Override
    public void restore() {
    	pLikes=(Double[][])storedpLikes.clone();
        logP = storedLogP;
        migrationCounts=migrationCountsStored.clone();
        nodeTypes=nodeTypesStored.clone();
        //migrationModelVolz=storedMigrationModelVolz;
        super.restore();
        //System.out.println("Number of nodes: "+mtTree.getNodeCount());
        //for (int i = 0; i<mtTree.getNodeCount(); i++) {
        	 //System.out.println("Index: "+i);
        	//((MultiTypeNodeVolz) mtTree.getNode(i)).setNodeType(nodeTypes[i]);
        //}
    }
    
    public void init(PrintStream out) throws RuntimeException {
    	try {
    		((Tree) mtTree).init(out);
    	} catch (Exception ex) {
            System.out.println("Error in initialzing structure coalescent density.");
        }
    }
    
    @Override
    /** Print tree with demes represented as names instead of numbers **/
    public void log(final long nSample, final PrintStream out) {
    	double prob=lineageProbsList.get(lineageProbsList.size()-1)[0];
        if (prob==prob){
        	//System.out.println("Set all demes called from log "+prob+"\n");
        	setAllDemes();
        	out.print("tree STATE_" + nSample + " = ");
        	out.print(toSortedNewickVolz(mtTree.getRoot(), new int[1])+";");
        	loggable=true;
        }else{ loggable=false;}
    }
    
    public String toSortedNewickVolz(Node node, int[] iMaxNodeInClade) {
        StringBuilder buf = new StringBuilder();
        if (node.getLeft() != null) {
            buf.append("(");
            String sChild1 = toSortedNewickVolz(node.getLeft(), iMaxNodeInClade);
            int iChild1 = iMaxNodeInClade[0];
            if (node.getRight() != null) {
                String sChild2 = toSortedNewickVolz(node.getRight(), iMaxNodeInClade);
                int iChild2 = iMaxNodeInClade[0];
                if (iChild1 > iChild2) {
                    buf.append(sChild2);
                    buf.append(",");
                    buf.append(sChild1);
                } else {
                    buf.append(sChild1);
                    buf.append(",");
                    buf.append(sChild2);
                    iMaxNodeInClade[0] = iChild1;
                }
            } else {
                buf.append(sChild1);
            }
            buf.append(")");
            if (getID() != null) {
                buf.append(node.getNr()+1);
            }
        } else {
            iMaxNodeInClade[0] = node.getNr();
            buf.append(node.getNr() + 1);
        }

        //if (printMetaData) {
        int nodeT=nodeTypes[((MultiTypeNodeVolz) node).getNr()];

        String loc;
        if (nodeT>(mtTree.getTypeList().size()-1)) loc="Unsampled"+(nodeT-mtTree.getTypeList().size());
        else loc=mtTree.getTypeList().get(nodeT);
        buf.append(("[&location="+loc+"]"));
        //buf.append(("[&location="+mtTree.getTypeList().get(nodeTypes[((MultiTypeNodeVolz) node).getNr()])+"]"));
        //}
        buf.append(":").append(node.getLength());
        return buf.toString();
    }
    
    
    
    /**
     * Used to log numbers of migration counts.
     */
    public int[] getCounts() {
        return migrationCounts;
    }
    
    /**
     * Used to log numbers of migration counts: how many demes are there?
     */
    public int getNDemes() {
        return nDemes;
    }
    
    /** Do you sample migration events? **/
    public void setSampleMig(boolean b) {
    	sampleMig=b;
    }



    public void getRootProbs(Double[] probs){
        MultiTypeNodeVolz root = (MultiTypeNodeVolz) mtTree.getRoot();
        Double[] rootProbs = pLikes[root.getNr()];
        System.arraycopy(rootProbs, 0, probs, 0, rootProbs.length);
    }

    /** obtain partial likelihoods **/
    public Double[] getLikes(int nodeNum) {
        return pLikes[nodeNum];
    }
    
    
    /** set partial likelihoods **/
    public void setLikes(Double[] likes, int nodeNum) {
        //startEditing();
        this.pLikes[nodeNum] = likes;
    }
    
    /**
     * Sample the number of virtual events to occur along branch.
     * 
     * General strategy here is to:
     * 1. Draw u from Unif(0,1),
     * 2. Starting from zero, evaluate P(n leq 0|a,b) up until n=thresh
     * or P(n leq 0|a,b)>u.
     * 3. If P(n leq 0|a,b) has exceeded u, use that n. If not, use rejection
     * sampling to draw conditional on n being >= thresh.
     * 
     * @param typeStart Type at start (bottom) of branch
     * @param typeEnd Type at end (top) of branch
     * @param muL Expected unconditioned number of virtual events
     * @param Pba Probability of final type given start type
     * @param migrationModel Migration model to use.
     * @param sym flag to say whether the migration matrix is symmetric
     * @return number of virtual events.
     */
    private int drawEventCount(int typeStart, int typeEnd, double muL, double Pba, MigrationModelVolz migrationModelVolz, boolean sym) {
    	//System.out.println("draw event count started\n");
        int nVirt = 0;
        long nVirt2=0;
        double u = Randomizer.nextDouble();
        double P_low_given_ab = 0.0;
        double acc = - muL - Math.log(Pba);
        double log_muL = Math.log(muL);
        //boolean doExit=false;
        int limitNum=100000;
        //int topNum=1;
        //System.out.println("typeStart "+typeStart+" typeEnd: "+typeEnd+" muL "+muL+" Pba "+Pba);
        //System.out.println("u "+u+" Summation: "+P_low_given_ab+" acc "+acc+" migration rates "+migrationModelVolz.getRate(0, 1)+" "+migrationModelVolz.getRate(1, 0)+" log_MuL "+log_muL);
        
        do {
        	//try{
        		//System.out.println("a");
        		P_low_given_ab += Math.exp(Math.log(migrationModelVolz.getRpowN(nVirt, sym).get(typeStart, typeEnd)) + acc);
        		//System.out.println("b");
        		//System.out.println("nVirt "+nVirt+" Summation: "+P_low_given_ab+" acc "+acc+" RpowN_a_b "+migrationModelVolz.getRpowN(nVirt, sym).get(typeStart, typeEnd));
        		//System.out.println("RpowN: "+migrationModelVolz.getRpowN(nVirt, sym).get(0, 0)+" "+migrationModelVolz.getRpowN(nVirt, sym).get(0, 1)+" "+migrationModelVolz.getRpowN(nVirt, sym).get(1, 0)+" "+migrationModelVolz.getRpowN(nVirt, sym).get(1, 1)+" ");
        	//}catch(Exception e){
        	//	doExit=true;
        	//}
        	//if(!doExit){
        		if (P_low_given_ab>u){
        			//if(nVirt>topNum){
        				//System.out.println("new top for nVirt "+nVirt);
        				//topNum=nVirt;
        			//}
        			//System.out.println("draw event count finished\n");
        			return nVirt;
        		}
        		//System.out.println("c");
        		nVirt += 1;
        		acc += log_muL - Math.log(nVirt);
        		//System.out.println("nVirt "+nVirt);
        	//}
        } while ((migrationModelVolz.RpowSteadyN(sym)<0 || nVirt<migrationModelVolz.RpowSteadyN(sym)) && (nVirt<limitNum));
        //System.out.println("out of do-while, nVirt "+nVirt);
        if(nVirt==limitNum){System.out.println("Warning: sampling ancestral states and migration counts might be numerically unstable due to the migration matrix. It is suggested to try defining more strict migration rate priors, a simpler migration model, or fewer demes.");}
        //if(nVirt>topNum){
        //	System.out.println("new top for nVirt "+nVirt);
		//	topNum=nVirt;
        //}
        
        int thresh = nVirt;
        
        // P_n_given_ab constant for n>= thresh: only need
        // to sample P(n|n>=thresh)
        do {
        	nVirt2=Randomizer.nextPoisson(muL);
            nVirt = (int) nVirt2;
            //System.out.println("Second Do while, nVirt "+nVirt+" thresh "+thresh);
        } while (nVirt < thresh);
        
        //System.out.println("draw event count finished\n");
        return nVirt;
    }
    
    /**
     * Sample migration events on a branch. Uses the combined
     * uniformization/forward-backward approach of Fearnhead and Sherlock (2006)
     * to condition on both the beginning and end states.
     *
     * @param srcNode
     * @return Probability of new state.
     * @throws beast.base.evolution.operator.UniformizationRetypeOperator.NoValidPathException
     */
    protected void drawMigrationTypes(int n, int typeStart, int typeEnd, double muL, double Pba, MigrationModelVolz migrationModelVolz, boolean sym ) {
    	//System.out.println("draw migration types started\n");

        // Abort if transition is impossible.
        if (Pba == 0.0)
            return ;
        
        // Catch for numerical errors
        if (Pba>1.0 || Pba<0.0) {
            System.err.println("Warning: matrix exponentiation resulted in rubbish.  Aborting move.");
            System.exit(0);
        }
        
        // Select number of virtual events:
        int nVirt = n;
        if(n>1000000)System.out.println("Warning: number of migration events on a single branch>1000000. This is usually unrealistic and makes sampling states very slow. Try to set a lower/stricter prior for migration rates\n");

        // Sample type changes along branch using FB algorithm:
        int [] subsTypes = new int[migrationModelVolz.getNDemes()*migrationModelVolz.getNDemes()];
        int prevType = typeStart;
        
        for (int i = 1; i<=nVirt; i++) {
        	//if(i%1000000==0) System.out.println(" "+i+"\n");
            double u2 = Randomizer.nextDouble()*migrationModelVolz.getRpowN(nVirt-i+1, sym).get(prevType, typeEnd);
            int c;
            boolean fellThrough = true;
            for (c = 0; c<migrationModelVolz.getNDemes(); c++) {
                u2 -= migrationModelVolz.getR(sym).get(prevType,c)*migrationModelVolz.getRpowN(nVirt-i, sym).get(c,typeEnd);
                if (u2<0.0001) {
                    fellThrough = false;
                    break;
                }
            }
            
            // Check for FB algorithm error:
            if (fellThrough) {
                double sum1 = migrationModelVolz.getRpowN(nVirt-i+1, sym).get(prevType, typeEnd);
                double sum2 = 0;
                for (c = 0; c<migrationModelVolz.getNDemes(); c++) {
                    sum2 += migrationModelVolz.getR(sym).get(prevType,c)*migrationModelVolz.getRpowN(nVirt-i, sym).get(c,typeEnd);
                }
                
                System.err.println("Warning: FB algorithm failure.  Exiting program.");
                System.out.println("sum1: "+sum1+"  sum2: "+sum2+"\n");
                System.exit(0);
                //return Double.NEGATIVE_INFINITY;
            }

            //types[i-1] = c;
            subsTypes[prevType*migrationModelVolz.getNDemes() + c]+=1;
            prevType = c;
            //System.out.println(" "+i+"\n");
        }
        for (int i=0;i<(migrationModelVolz.getNDemes()*migrationModelVolz.getNDemes());i++) migrationCounts[i]+=subsTypes[i];
        //System.out.println("draw migration types finished\n");
        //System.out.println("For a branch. S deme: "+typeStart+" end deme: "+typeEnd+" mu*t: "+muL+" substitutions"+subsTypes[0]+" "+subsTypes[1]+" "+subsTypes[2]+" "+subsTypes[3]+" "+"\n");
    }
    
    
    /**
     * set one deme at the root picked at random from the probability distribution of demes at the root.
     */
    public void setRootDeme() {
    	int type=-1;
    	MultiTypeNodeVolz root=(MultiTypeNodeVolz) mtTree.getRoot();
    	Double[] probs = pLikes[root.getNr()];
    	//Double[] probs = lineageProbsList.get(lineageProbsList.size()-1);
    	//System.out.println("\n New root\n");
    	double x = Randomizer.nextDouble();
    	double sum=0.0;
    	for (int i = 0; i<probs.length; i++) {
    		//System.out.println("Probability at index "+i+" = "+probs[i]);
    		sum+=probs[i];
    		if (x<sum){
    			type=i;
    			break;
    		}
    	}
    	if (type==-1) {
    		System.out.println("Something is wrong with probabilities at the root.");
    		for (int i = 0; i<probs.length; i++) {
    			System.out.println("Probability at index "+i+" = "+probs[i]+"\n");
        	}
    		System.exit(0);
    	}
        //MultiTypeNodeVolz root=(MultiTypeNodeVolz) mtTree.getRoot();
        //root.setNodeType(type);
    	//System.out.println("\n Root type: \n"+type);
        nodeTypes[root.getNr()]=type;
    }
    
    
    /**
     * set all demes at internal nodes, picking at random from their probability conditioned on their subtree leaves, 
     * and conditioned on the deme sampled for their parent.
     */
    public void setAllDemes() {
    	if (sampleMig){
    		migrationModelVolz.updateMatrices();
    		migrationCounts = new int[migrationModelVolz.getNDemes()*migrationModelVolz.getNDemes()];
    	}
    	setRootDeme();
    	MultiTypeNodeVolz root=(MultiTypeNodeVolz) mtTree.getRoot();
    	//System.out.println("Root has deme "+root.getNodeType()+"\n");
    	//Then make iteration
    	if ((root.getLeft()!=null)){// && (!root.getLeft().isLeaf())
    		Node LC= root.getLeft();
        	double LCTime= root.getHeight() - LC.getHeight();
        	setDemeIterative(root.getLeft(),LCTime, nodeTypes[((MultiTypeNodeVolz) root).getNr()] );//root.getNodeType()
    	}
    	
    	if ((root.getRight()!=null)){ //&& (!root.getRight().isLeaf())
    		Node RC= root.getRight();
        	double RCTime= root.getHeight() - RC.getHeight();
        	setDemeIterative(root.getRight(),RCTime, nodeTypes[((MultiTypeNodeVolz) root).getNr()] );
    	}
    }
    

    
    /**
     * Set the deme for a node, and then call the function iteratively for the leaves.
     * and conditioned on the deme sampled for their parent.
     */
    public void setDemeIterative(Node node, double time, int parentDeme) {
    	//System.out.println("New node, set deme iterative\n");
    	//get probability matrix for the considered branch
    	int nTypes = migrationModelVolz.getNDemes();
    	int nRates;
    	if (migrationModelVolz.getUniform()){
    		nRates=2;
    	}else{
    		nRates=nTypes*nTypes;
    	}
    	double[] pMatrix = new double[nRates];
    	if (migrationModelVolz.getJblas()==false){
    		if (migrationModelVolz.getUniform()){
    			pMatrix[0]=(1.0/nTypes) +((nTypes-1.0)*Math.exp(-migrationModelVolz.getRate(0, 1)*time*nTypes))/nTypes ;
    			pMatrix[1]=(1.0/nTypes) -(Math.exp(-migrationModelVolz.getRate(0, 1)*time*nTypes)/nTypes) ;
    		}else{
    			//BEAST2 exponentiation
        		//System.out.println("Use BEAST exponentiation");
    			migrationModelVolz.getTransitionProbabilities(time, pMatrix);
    		}
		}else{
			if (migrationModelVolz.getUniform()){
    			pMatrix[0]=(1.0/nTypes) +((nTypes-1.0)*Math.exp(-migrationModelVolz.getRate(0, 1)*time*nTypes))/nTypes ;
    			pMatrix[1]=(1.0/nTypes) -(Math.exp(-migrationModelVolz.getRate(0, 1)*time*nTypes)/nTypes) ;
    		}else{
	            //use jblas to calculate pMatrix, the probability matrix of migration events
				//System.out.println("Use JBLAS");
	            DoubleMatrix Qsupp = new DoubleMatrix(nTypes, nTypes);
	            for (int i = 0; i < nTypes; i++) {
	            	Qsupp.put(i,i, 0.0);
	            	for (int j = 0; j < nTypes; j++) {
	            		if (i != j) {
	            			Qsupp.put(i, j, migrationModelVolz.getRate(i, j)*time);
	            			Qsupp.put(i, i, Qsupp.get(i, i) - Qsupp.get(i, j));
	            		}
	            	}
	            }
	            DoubleMatrix Fsupp = MatrixFunctions.expm(Qsupp);
	            for (int t = 0; t<nTypes; t++) {
	            	for (int t2 = 0; t2<nTypes; t2++) {
	    				pMatrix[(t*nTypes) + t2]=Fsupp.get(t, t2);
	    			}
	            }
    		}
		}
    	//System.out.println("Calculated p matrix\n");
    	//get the lower likelihoods for the node
    	Double[] likes=pLikes[node.getNr()];
    	
    	//calculate probabilities for node location
    	double[] probs = new double[nTypes];
    	double sum=0.0;
    	//System.out.println("likes size "+likes.length+"\n");
    	//System.out.println("pMatrix size "+pMatrix.length+"\n");
    	if (migrationModelVolz.getUniform()){
    		for (int t = 0; t<nTypes; t++) {
    			if (t==parentDeme){
    				probs[t]=likes[t]*pMatrix[0];
    			}else{
    				probs[t]=likes[t]*pMatrix[1];
    			}
        		sum+=probs[t];
    		}
    	}else{
    		for (int t = 0; t<nTypes; t++) {  //NICOLA consider the case uniform rates here
        		//System.out.println("Index t "+t+"\n");
        		//System.out.println("pMatrix index "+((t*nTypes) + parentDeme)+"\n");
        		probs[t]=likes[t]*pMatrix[(t*nTypes) + parentDeme];
        		sum+=probs[t];
        	}
    	}
    	if(sum<0.0000000001){
    		System.out.println("sum: "+sum+" "+parentDeme+" "+time+" "+migrationModelVolz.getJblas());
    		for (int t = 0; t<nTypes; t++) {
        		//System.out.println("Index t "+t+"\n");
        		//System.out.println("pMatrix index "+((t*nTypes) + parentDeme)+"\n");
    			System.out.println(t+" "+probs[t]+" "+likes[t]);
        	}
    	}
    	for (int t = 0; t<nTypes; t++) {
    		probs[t]=probs[t]/sum;
    	}
    	
    	//Set node type!
    	//System.out.println("Set node type");
    	double x = Randomizer.nextDouble();
    	int type=-1;
    	sum=0.0;
    	//System.out.println("probs: ");
    	for (int i = 0; i<probs.length; i++) {
    		sum+=probs[i];
    		//System.out.println(probs[i]);
    		if (x<sum){
    			type=i;
    			break;
    		}
    	}
    	if (type==-1) {
    		System.out.println("Something is wrong with probabilities.");
    		System.exit(0);
    	}
//    	if (node.isLeaf()) {
//    		System.out.println("leaf "+node.getNr()+" type "+type);
//    		for (int i = 0; i<probs.length; i++) {
//    			System.out.println(" prob "+i+" "+probs[i]);
//    		}
//    	}
		//((MultiTypeNodeVolz) node).setNodeType(type);
    	if ((!node.isLeaf()) || (((MultiTypeNodeVolz) node).getNodeType()==-1)) {/////////////////////////////
    		//((MultiTypeNodeVolz) node).setNodeType(type);
    		nodeTypes[node.getNr()]=type;
    	}else{
    		nodeTypes[node.getNr()]=((MultiTypeNodeVolz) node).getNodeType();
    	}
    	//System.out.println("Set node type done");
    	
    	if (sampleMig){
    		//System.out.println("Start sampling number of mig");
    		//use symmetrized rates?
    		boolean sym = false;
    		double muL=migrationModelVolz.getMu(sym)*time;
    		double Pba;
    		if (migrationModelVolz.getUniform()){
    			if (type==parentDeme) Pba= pMatrix[0];
    			else Pba= pMatrix[1];
    		}else Pba= pMatrix[(type*nTypes) + parentDeme];
    		//System.out.println("P matrix: "+pMatrix[0]+" "+pMatrix[1]+" "+pMatrix[2]+" "+pMatrix[3]+" ");
    		//System.out.println("muL: "+muL+" time: "+time+"  Pba: "+Pba+"  type: "+type+"  parentDeme: "+parentDeme);
    		//calculate number of migrations on branch
    		int n= drawEventCount(type, parentDeme, muL, Pba, migrationModelVolz, sym);
    		//System.out.println("Done counts");
    		//sample migration events and add them to the global count
    		drawMigrationTypes(n, type, parentDeme, muL, Pba, migrationModelVolz, sym);
    		//System.out.println("End sampling number of mig");
    	}
    	//System.out.println("Migration type counts\n"+Arrays.toString(migrationCounts));		
    	//System.out.println("End node, set deme iterative, starting iteration\n");
    	//Then make iteration
    	if (node.getLeft()!= null){
    		Node LC= node.getLeft();
    			double LCTime= node.getHeight() - LC.getHeight();
    			//System.out.println("launch first iteration\n");
    			setDemeIterative(LC ,LCTime, nodeTypes[((MultiTypeNodeVolz) node).getNr()]);//((MultiTypeNodeVolz) node).getNodeType()
    	}
    	
    	if (node.getRight()!= null){
    		Node RC= node.getRight();
    			double RCTime= node.getHeight() - RC.getHeight();
    			//System.out.println("launch second iteration\n");
    			setDemeIterative(RC ,RCTime, nodeTypes[((MultiTypeNodeVolz) node).getNr()]);
    	}
    	
    }
    
    

    // Empty constructor as required:
    public StructuredCoalescentTreeDensityVolz() { };

    @Override
    public void initAndValidate() {
        migrationModelVolz = migrationModelInput.get();
        mtTree = mtTreeInput.get();
        checkValidity = checkValidityInput.get();
        //sampleMig = sampleMigInput.get();
        sampleMig=false;
        migrationCounts = new int[migrationModelVolz.getNDemes()*migrationModelVolz.getNDemes()];//mtTree.getNTypes()*mtTree.getNTypes()
        nodeTypes = new int[mtTree.getNodeCount()];
        eventList = new ArrayList<SCEvent>();
      	lineageCountList = new ArrayList<Double[]>();
        lineageCountSquareList = new ArrayList<Double[]>();
        lineageProbsList = new ArrayList<Double[]>();
        lineageCountListAfter = new ArrayList<Double[]>();
        lineageCountSquareListAfter = new ArrayList<Double[]>();
        pLikes = new Double[mtTree.getNodeCount()][migrationModelVolz.getNDemes()];//mtTree.getNTypes()

    }

    @Override
    public double calculateLogP() {
    	
    	//System.out.println("Calculating logP\n");
        // Check validity of tree if required:
        if (checkValidity && !mtTree.isValid())
            return Double.NEGATIVE_INFINITY;
        
        
        if (migrationModelVolz.getJblas()==false){
        	migrationModelVolz.updateMatrices();
        }
        
        int nTypes = migrationModelVolz.getNDemes();
        nDemes=nTypes;
        
        // Ensure sequence of events is up-to-date:
        updateEventSequence();
        
        double prob=lineageProbsList.get(lineageProbsList.size()-1)[0];
        if (prob!=prob){
        	loggable=false;
        	return Double.NEGATIVE_INFINITY;
        }else{ loggable=true;}

        // Start from the tips of the tree, working up.
        logP = 0;

        
        // Note that the first event is always a sample. We begin at the first
        // _interval_ and the event following that interval.
        for (int eventIdx = 1; eventIdx<eventList.size(); eventIdx++) {
        	//System.out.println("Event index: "+eventIdx);
        	
            SCEvent event = eventList.get(eventIdx);
            
            //Counts of expected lineages and squared ones for present and next event times.
            Double[] lineageCount = lineageCountListAfter.get(eventIdx-1);
            Double[] lineageCountSquare = lineageCountSquareListAfter.get(eventIdx-1);
            Double[] lineageCount2 = lineageCountList.get(eventIdx);
            Double[] lineageCountSquare2 = lineageCountSquareList.get(eventIdx);
            
            double delta_t = event.time - eventList.get(eventIdx-1).time;
            
        	//calculate sub-intervals of time to be considered
        	double delta_t1=0.0, delta_t2=0.0;
//        	double totPopSize=migrationModelVolz.getTotalPopSize();
        	delta_t1=delta_t/2.0;
    		delta_t2=delta_t/2.0;
        	
        	//add first interval half contribution to likelihood
        	double totRate=0.0;
        	if (delta_t1>0) {
        		//calculate total coalescent rate at lower sub-interval
        		for (int c = 0; c<nTypes; c++) {
        			double N = migrationModelVolz.getPopSize(c);
            		totRate+=(((lineageCount[c]*lineageCount[c])-lineageCountSquare[c])/(2.0*N));
            	}
        		logP += -delta_t1*totRate;
        	}
			
        	//add second interval half contribution to likelihood
        	if (delta_t2>0) {
        		//calculate total coalescent rate at higher sub-interval
            	totRate=0.0;
            	for (int c = 0; c<nTypes; c++) {
            		double N = migrationModelVolz.getPopSize(c);
            		totRate+=(((lineageCount2[c]*lineageCount2[c])-lineageCountSquare2[c])/(2.0*N));
            	}
        		logP += -delta_t2*totRate;
        	}
        	
            // Event contribution:
            switch (event.kind) {
                case COALESCE:
                	Double[] lineageProbs1 = new Double[nTypes];
                    Double[] lineageProbs2 = new Double[nTypes];
                    Double[] lineageProbs = lineageProbsList.get(eventIdx);	
    				for (int c = 0; c<nTypes; c++) {
    					lineageProbs1[c] = lineageProbs[c]; 
    					lineageProbs2[c] = lineageProbs[nTypes+c]; 
    				}
                	double coalRate=0.0;
            		for (int c = 0; c<nTypes; c++){
            			double N = migrationModelVolz.getPopSize(c);
            			coalRate+= (lineageProbs1[c]*lineageProbs2[c])/N;
            		}			
                    logP += Math.log(coalRate);
                    break;

                case SAMPLE:
                    // Do nothing here: only effect of sampling event is
                    // to change the lineage counts in subsequent intervals.
                    break;
            }
        }
        //System.out.println("\n\n Calculations logP ended. Final score:");
		//System.out.println(logP);
        return logP;
    }

    
    
    /**
     * Determines the sequence of coalescence and sampling events
     * which make up the Volz tree.
     */
    public void updateEventSequence() {
    	
    	//System.out.println("Update sequence events\n");
        // Clean up previous list:
        eventList.clear();
        lineageCountList.clear();
        lineageCountSquareList.clear();
        lineageCountListAfter.clear();
        lineageCountSquareListAfter.clear();
        lineageProbsList.clear();
        Node rootNode = mtTree.getRoot();
        int nTypes= migrationModelVolz.getNDemes();

        // Initialise map of active nodes to active change indices:
        Map<Node, Integer> changeIdx = new HashMap<Node, Integer>();
        changeIdx.put(rootNode, -1);

        // Calculate event sequence:
        while (!changeIdx.isEmpty()) {

            SCEvent nextEvent = new SCEvent();
            nextEvent.time = Double.NEGATIVE_INFINITY;
            nextEvent.node = rootNode; // Initial assignment not significant

            // Determine next event
            for (Node node : changeIdx.keySet())
                if (changeIdx.get(node)<0) {
                    if (node.isLeaf()) {
                        // Next event is a sample
                        if (node.getHeight()>nextEvent.time) {
                            nextEvent.time = node.getHeight();
                            nextEvent.kind = SCEventKind.SAMPLE;
                            //nextEvent.type = nodeTypes[((MultiTypeNodeVolz) node).getNr()];//((MultiTypeNodeVolz)node).getNodeType();
                            nextEvent.type = ((MultiTypeNodeVolz)node).getNodeType();
                            nextEvent.node = node;
                        }
                    } else {
                        // Next event is a coalescence
                        if (node.getHeight()>nextEvent.time) {
                            nextEvent.time = node.getHeight();
                            nextEvent.kind = SCEventKind.COALESCE;
                            nextEvent.node = node;
                            nextEvent.type = nodeTypes[((MultiTypeNodeVolz) node).getNr()];
                        }
                    }
                }
            

            // Update active node list (changeIdx) and lineage count appropriately:
            switch (nextEvent.kind) {
                case COALESCE:
                	Node leftChild = nextEvent.node.getLeft();
                    Node rightChild = nextEvent.node.getRight();

                    changeIdx.remove(nextEvent.node);
                    changeIdx.put(leftChild, -1);
                    changeIdx.put(rightChild, -1);
                    break;

                case SAMPLE:
                    changeIdx.remove(nextEvent.node);
                    break;
            }

            // Add event to list:
            eventList.add(nextEvent);
        }
        
        //System.out.println("Step 0\n");
        
        
        // Reverse event and lineage count lists (order them from tips to root):
        eventList = Lists.reverse(eventList);
        
        
        // Initialise lineage count per colour array:
        SCEvent nextEvent = eventList.get(0);
        double coalTime=nextEvent.time;
        Double[] lineageCount = new Double[nTypes];
        for (int c = 0; c<nTypes; c++) 
        	if (nextEvent.type==c) {
        		lineageCount[c]=1.0;
        	}else if(nextEvent.type==-1){
        		lineageCount[c]=1.0/nTypes;
        	}else{
        		lineageCount[c]=0.0;
        	}
        
        //Initialize first node
        MultiTypeNodeVolz firstNode = (MultiTypeNodeVolz) nextEvent.node;
        firstNode.setCacheIndex(0);
        setLikes(Arrays.copyOf(lineageCount, lineageCount.length),firstNode.getNr());
        //firstNode.setLikes(Arrays.copyOf(lineageCount, lineageCount.length));
        
        //Initialize square count
        Double[] lineageCountSquare = new Double[nTypes];
        for (int c = 0; c<nTypes; c++) lineageCountSquare[c]=lineageCount[c]*lineageCount[c];
        lineageCountList.add(null);
        lineageCountSquareList.add(null);
        lineageCountListAfter.add(Arrays.copyOf(lineageCount, lineageCount.length));
        lineageCountSquareListAfter.add(Arrays.copyOf(lineageCount, lineageCount.length));
        //initialize cache of probabilities for lineages to be in demes
        List<Double[]> lineageProbs = new ArrayList<Double[]>();
        lineageProbs.add(Arrays.copyOf(lineageCount, lineageCount.length));
        lineageProbsList.add(null);
        //List to keep track of how many lineages are being cached on the same probability vector.
        List<Integer> numCached = new ArrayList<Integer>();
        numCached.add(1);
        //List to keep track of which types the cache represent, if they are samples just inserted
        List<Integer> typeCached = new ArrayList<Integer>();
        typeCached.add(nextEvent.type);
        
        //Count expected number of lineages in each deme, etc, going from bottom to top
        int nRates;
        if (migrationModelVolz.getUniform()) nRates=2;
        else nRates=nTypes*nTypes;
        double[] pMatrix = new double[nRates];
        Double[] newLineageCount = new Double[nTypes];
        
        //System.out.println("Step 1\n");
        
        //boolean useJblas=false;
        if (migrationModelVolz.getJblas()==false){
	        migrationModelVolz.getTransitionProbabilities(1.0, pMatrix);
	        for (int i = 0; i < nTypes; i++) {
	        	double totFrom=0.0;
	        	for (int j = 0; j < nTypes; j++) {
	        		if (migrationModelVolz.getUniform()){
	        			if (i==j) totFrom+=pMatrix[0];
	        			else totFrom+=pMatrix[1];
	        		}else totFrom+=pMatrix[(i*nTypes) + j];
	    		}
	        	if ((totFrom<0.9)||(totFrom>1.1)){
	        		migrationModelVolz.setJblas(true);
	        		//System.out.println("problems with beast exponentiation, using jblas");
	        	}
	//        	else{
	//        		System.out.println("beast exponentiation ok");
	//        	}
	        }
        }
        
        //System.out.println("Step 2\n");

        for (int c = 1; c<eventList.size(); c++) {
        	nextEvent = eventList.get(c);
        	//System.out.println("New iteration on events . nTypes: "+nTypes+" \n" );
        	if ((nextEvent.time != coalTime)||(nextEvent.kind == SCEventKind.COALESCE)){
        		double delta_t = nextEvent.time - coalTime;
        		
        		if (migrationModelVolz.getJblas()==false){
        			//BEAST2 exponentiation////////////////////////////////////////////////////////
        			//System.out.println("Jblas=false, calculate trans probs\n");
        			migrationModelVolz.getTransitionProbabilities(delta_t, pMatrix);
        			//System.out.println("calculated trans probs\n");
        			//System.out.println("Using JBLAS\n");
        		}else{
        			if (migrationModelVolz.getUniform()){
        				System.out.println("Using JBLAS with uniform matrix?\n");
        				System.exit(0);
        			}
        			//System.out.println("Jblas=true, calculate trans probs\n");
	                //use jblas to calculate pMatrix, the probability matrix of migration events
	                DoubleMatrix Qsupp = new DoubleMatrix(nTypes, nTypes);
	                for (int i = 0; i < nTypes; i++) {
	                	Qsupp.put(i,i, 0.0);
	                	for (int j = 0; j < nTypes; j++) {
	                		if (i != j) {
	                			Qsupp.put(i, j, migrationModelVolz.getRate(i, j)*delta_t);
	                			Qsupp.put(i, i, Qsupp.get(i, i) - Qsupp.get(i, j));
	                		}
	                	}
	                }
	                DoubleMatrix Fsupp = MatrixFunctions.expm(Qsupp);
	                for (int t = 0; t<nTypes; t++) {
	                	for (int t2 = 0; t2<nTypes; t2++) {
	        				pMatrix[(t*nTypes) + t2]=Fsupp.get(t, t2);
	        			}
	                }
	                //System.out.println("Using BEAST\n");
	                //System.out.println("calculated trans probs jblas\n");
        		}
        		//System.out.println("Matrix exponentiation is:\n");
//        		for (int t = 0; t<pMatrix.length; t++) {
//        			System.out.println("Index "+t+" "+pMatrix[t]);
//        		}
        		
        		//find new expected counts of lineages in demes
        		//System.out.println("Lineage counts\n");
        		for (int t = 0; t<nTypes; t++) {
        			newLineageCount[t]=0.0;
        			for (int t2 = 0; t2<nTypes; t2++) {
        				if (migrationModelVolz.getUniform()){
        					if (t2==t){
        						newLineageCount[t]+=lineageCount[t2]*pMatrix[0];
        					}else{
        						newLineageCount[t]+=lineageCount[t2]*pMatrix[1];
        					}
        				}else{
        					newLineageCount[t]+=lineageCount[t2]*pMatrix[(t2*nTypes) + t];
        				}
        			}	
        			
        		}
        		for (int t = 0; t<nTypes; t++) lineageCount[t]=newLineageCount[t];
        		lineageCountList.add(Arrays.copyOf(lineageCount, lineageCount.length));
        		//System.out.println("Lineage counts done\n");
        		
        		//System.out.println("Lineage counts squared\n");
        		//find new expected squared counts of lineages in demes
        		for (int t = 0; t<nTypes; t++) lineageCountSquare[t]=0.0;
        		for (int ca = 0; ca<numCached.size(); ca++) {
        			if (numCached.get(ca)>0){
        				Double[] prob = lineageProbs.get(ca);
            			Double[] newProbs = new Double[nTypes];
            			for (int t = 0; t<nTypes; t++) {
            				double newProb=0.0;
            				if (migrationModelVolz.getUniform()) newProb+=prob[t]*pMatrix[0]+(1-prob[t])*pMatrix[1];
            				else {
	            				for (int t2 = 0; t2<nTypes; t2++) {
	                					newProb+=prob[t2]*pMatrix[(t2*nTypes) +t];
	            				}
            				}
            				//System.out.println("Updated prob after lineage: "+newProb);
            				newProbs[t]=newProb;
            			}
            			lineageProbs.set(ca, Arrays.copyOf(newProbs, nTypes));
            			for (int t = 0; t<nTypes; t++) {
            				lineageCountSquare[t]+=newProbs[t]*newProbs[t]*numCached.get(ca); 
            			}
        			}
        		}
        		//System.out.println("Lineage counts squared done\n");
        		
        		lineageCountSquareList.add(Arrays.copyOf(lineageCountSquare, lineageCountSquare.length));
        		
        		//update counts after the event (not used for present coalescing rates)
        		switch (nextEvent.kind) {
                case COALESCE:
                	//System.out.println("Coalescent new time\n");
                    MultiTypeNodeVolz leftChild = (MultiTypeNodeVolz) nextEvent.node.getLeft();
                    MultiTypeNodeVolz rightChild = (MultiTypeNodeVolz) nextEvent.node.getRight();
                    int index1=leftChild.getCacheIndex();
                    int index2=rightChild.getCacheIndex();
                    
                    //pass probability vectors right before coalescence for likelihood calculation
                    Double[] lineageProbsPair = new Double[2*nTypes];
                    for (int t = 0; t<nTypes; t++) lineageProbsPair[t]=lineageProbs.get(index1)[t];
                    for (int t = 0; t<nTypes; t++) lineageProbsPair[t+nTypes]=lineageProbs.get(index2)[t];
                    lineageProbsList.add(Arrays.copyOf(lineageProbsPair, 2*nTypes));
                    
                    //calculate the probability vector for the new coalesced lineage, following the coalescent event
                    double tot=0.0;
                    for (int t = 0; t<nTypes; t++) tot+= (lineageProbs.get(index1)[t]*lineageProbs.get(index2)[t]/migrationModelVolz.getPopSize(t));
//                    System.out.println("Coalescence");
//                    for (int t = 0; t<nTypes; t++) {
//                    	System.out.println("Coalescence, index "+t+" has prob1 "+lineageProbs.get(index1)[t]+" prob2 "+lineageProbs.get(index2)[t]+" popsize "+migrationModelVolz.getPopSize(t));
//                    }
                    Double[] newProb = new Double[nTypes];
                    for (int t = 0; t<nTypes; t++) newProb[t]=(lineageProbs.get(index1)[t]*lineageProbs.get(index2)[t]/migrationModelVolz.getPopSize(t))/tot;
                    
                    lineageProbs.add(Arrays.copyOf(newProb, nTypes));
                    setLikes(Arrays.copyOf(newProb, nTypes), nextEvent.node.getNr());
                    
                    MultiTypeNodeVolz coalesced = (MultiTypeNodeVolz) nextEvent.node;
                    coalesced.setCacheIndex(lineageProbs.size()-1);
                    numCached.add(1);
                    numCached.set(index1, numCached.get(index1)-1);
                    numCached.set(index2, numCached.get(index2)-1);
                    for (int t = 0; t<typeCached.size(); t++) typeCached.set(t,-2);
                    typeCached.add(-2);
                    //set node location according to bottom-to-top likelihood calculation- works well only for the root!
                    //double u = Randomizer.nextDouble();
                    //double sumProb=0.0;
                    //int newNodeType=-1;
                    //for (int t = 0; t<nTypes; t++) {
                    //	sumProb=sumProb+newProb[t];
                    //	if (sumProb>= u){
                    //		newNodeType=t;
                    //	}
                    //}
                    //coalesced.setNodeType(newNodeType);
                    //nodeTypes[coalesced.getNr()]=newNodeType;
                    
                    //Final update of counts, following the coalescent event
                    for (int t = 0; t<nTypes; t++) 
                    	lineageCount[t]=lineageCount[t]-(lineageProbs.get(index1)[t]+lineageProbs.get(index2)[t])+newProb[t];
                    for (int t = 0; t<nTypes; t++) 
                    	lineageCountSquare[t]=lineageCountSquare[t]-(lineageProbs.get(index1)[t]*lineageProbs.get(index1)[t]+lineageProbs.get(index2)[t]*lineageProbs.get(index2)[t])+newProb[t]*newProb[t];
                    
                    ///put probability distribution of root!!!
                    if (c==(eventList.size()-1)){
                    	lineageProbsList.add(Arrays.copyOf(newProb, nTypes));
//                    	System.out.println("Root\n");
//                    	for (int i = 0; i<nTypes; i++) {
//                			System.out.println("Root Probability at index "+i+" = "+lineageProbsList.get(lineageProbsList.size()-1)[i]+"\n");
//                    	}
                    }
                    	
                    //System.out.println("End coalescence\n");
                    
                    break;

                case SAMPLE:
                	//Adding a sample at a new time point
                	//System.out.println("New sample new time\n");
                	numCached.add(1);
                	for (int t = 0; t<typeCached.size(); t++) typeCached.set(t,-2);
                    typeCached.add(nextEvent.type);
                    lineageProbsList.add(null);
                    if (nextEvent.type==-1){
                    	for (int t = 0; t<nTypes; t++){
                    		lineageCount[t]+=1.0/nTypes;
                    		lineageCountSquare[t]+=1.0/(nTypes*nTypes);
                    	}
                    }else{
                    	lineageCount[nextEvent.type]+=1.0;
             			lineageCountSquare[nextEvent.type]+=1.0;
                    }
        			Double[] probs = new Double[nTypes];
        			for (int t = 0; t<nTypes; t++) 
        	        	if (nextEvent.type==t) {
        	        		probs[t]=1.0;
        	        	}else if(nextEvent.type==-1){
        	        		probs[t]=1.0/nTypes;
        	        	}else{
        	        		probs[t]=0.0;
        	        	}
        	        lineageProbs.add(Arrays.copyOf(probs, nTypes));
        	        setLikes(Arrays.copyOf(probs, nTypes), nextEvent.node.getNr());
        	        //((MultiTypeNodeVolz) nextEvent.node).setLikes(Arrays.copyOf(probs, nTypes));
        	        MultiTypeNodeVolz sampled = (MultiTypeNodeVolz) nextEvent.node;
                    sampled.setCacheIndex(lineageProbs.size()-1);
                    //System.out.println("End sample new time\n");
                    break;
        		}
        		
        		
        	}else{     //new sampling at the same time point
        		//System.out.println("New sampling same time\n");
        		lineageCountList.add(Arrays.copyOf(lineageCount, lineageCount.length));
    			lineageCountSquareList.add(Arrays.copyOf(lineageCountSquare, lineageCountSquare.length));
    	        lineageProbsList.add(null);
        		int type = nextEvent.type;
        		int found=0;
        		for (int t = 0; t<typeCached.size(); t++) {
        			if (type==typeCached.get(t)){//re-using the same cached probability values
        				found=1;
        				numCached.set(t,numCached.get(t)+1);
        				MultiTypeNodeVolz sampled = (MultiTypeNodeVolz) nextEvent.node;
                        sampled.setCacheIndex(t);
        				break;
        			}
        		}
        		Double[] probs = new Double[nTypes];
    			for (int t = 0; t<nTypes; t++) 
    	        	if (type==t) {
    	        		probs[t]=1.0;
    	        	}else if(type==-1){
    	        		probs[t]=1.0/nTypes;
    	        	}else{
    	        		probs[t]=0.0;
    	        	}
    			setLikes(Arrays.copyOf(probs, nTypes), nextEvent.node.getNr());
    			//((MultiTypeNodeVolz) nextEvent.node).setLikes(Arrays.copyOf(probs, nTypes));
        		if (found==0){         // a new leaf of a new location is added
        			typeCached.add(type);
        			numCached.add(1);
        	        lineageProbs.add(Arrays.copyOf(probs, nTypes));
        	        MultiTypeNodeVolz sampled = (MultiTypeNodeVolz) nextEvent.node;
                    sampled.setCacheIndex(lineageProbs.size()-1);
        		}
        		//System.out.println(type);
        		if (type==-1){
                	for (int t = 0; t<nTypes; t++){
                		lineageCount[type]+=1.0/nTypes;
                		lineageCountSquare[type]+=1.0/(nTypes*nTypes);
                	}
                }else{
                	lineageCount[type]+=1.0;
         			lineageCountSquare[type]+=1.0;
                }
        		//lineageCount[type]+=1.0;
    			//lineageCountSquare[type]+=1.0;
        		//System.out.println("end sampling same time\n");
        	}
        	
        	lineageCountListAfter.add(Arrays.copyOf(lineageCount, lineageCount.length));
            lineageCountSquareListAfter.add(Arrays.copyOf(lineageCountSquare, lineageCountSquare.length));
        	coalTime=nextEvent.time;
        	
        }
        //System.out.println("Ended iteration on events\n");
        
        //if(sampleMig) migrationCounts = new int[mtTree.getNTypes()*mtTree.getNTypes()];
        migrationCounts = new int[migrationModelVolz.getNDemes()*migrationModelVolz.getNDemes()];
        //pick location for all internal nodes and sample migration events if requested
        double prob=lineageProbsList.get(lineageProbsList.size()-1)[0];
        if (prob==prob){
        	//System.out.println("Set all demes called from end of update sequence events "+prob+"\n");
        	loggable=true;
        	setAllDemes();
        }else{ loggable=false;}
        //setAllDemes();
        //System.out.println("Updated sequence events. ");
    }

    
    @Override
    public boolean requiresRecalculation() {
        return true;
    }
    
    public Double getLogP() {
        return logP;
    }

    /**
     * Test likelihood result. Duplicate of JUnit test for debugging purposes.
     *
     * @param argv
     */
    public static void main(String[] argv) throws Exception {

        // Assemble test MultiTypeTree:
        String newickStr = "(T1:0.01,T2:0.01):0.0;";
               // "(((A[state=1]:0.25)[state=0]:0.25,B[state=0]:0.5)[state=0]:1.5,"
                //+"(C[state=0]:1.0,D[state=0]:1.0)[state=0]:1.0)[state=0]:0.0;";
       
        Taxon tax1 = new Taxon("T1");
        Taxon tax2 = new Taxon("T2");
        List<Taxon> taxList= new ArrayList<Taxon>();
        taxList.add(tax1);
        taxList.add(tax2);
        TaxonSet taxa = new TaxonSet();
        taxa.initByName("taxon",taxList);
        
        TraitSet typeTraitSet= new TraitSet();
        typeTraitSet.initByName("value", "T1=0, T2=0","traitname","type","taxa",taxa);

        MultiTypeTreeFromNewickVolz mtTree = new MultiTypeTreeFromNewickVolz();
        System.out.print("Initializing tree\n");
        mtTree.initByName(
                "newick", newickStr,
                "trait", typeTraitSet);
                //"typeLabel", "state");
                //,"nTypes", 2);
        System.out.print("Initialized tree\n");
        
        // Assemble migration model:
        System.out.print("Initializing rate matrix\n");
        RealParameter rateMatrix = new RealParameter();
        rateMatrix.initByName("value", "2.0 1.0");
        System.out.print("Initialized rate matrix\n");
        System.out.print("Initializing pop sizes\n");
        RealParameter popSizes = new RealParameter();
        popSizes.initByName("value", "5.0 10.0");
        System.out.print("Initialized pop sizes\n");
        System.out.print("Initializing mig model\n");
        MigrationModelVolz migrationModel = new MigrationModelVolz();
        migrationModel.initByName(
                "rateMatrix", rateMatrix,
                "popSizes", popSizes,"nTypes", 2);
        System.out.print("Initialized mig model\n");

        // Set up likelihood instance:
        System.out.print("Initializing tree prior\n");
        StructuredCoalescentTreeDensityVolz likelihood = new StructuredCoalescentTreeDensityVolz();
        likelihood.initByName(
                "migrationModelVolz", migrationModel,
                "multiTypeTreeVolz", mtTree);
        System.out.print("Initialized tree prior\n");

        //double expResult = -16.52831;  // Calculated by hand
        double result = likelihood.calculateLogP();

        //System.out.println("Expected result: "+expResult);
        System.out.println("Log likelihood result: "+result);
        //System.out.println("Difference: "+String.valueOf(result-expResult));

    }
}