package basta.distributions;

import basta.evolution.tree.MultiTypeNodeVolz;
import beast.base.evolution.tree.Node;
import com.google.common.collect.Lists;
import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

import java.io.PrintStream;
import java.util.*;

/**
 * Created by jessiewu on 28/01/2017.
 */
public class PureStructuredCoalescentTreeDensityVolz extends StructuredCoalescentTreeDensityVolz{

    public void initAndValidate() {
        super.initAndValidate();
        nDemes = migrationModelVolz.getNDemes();

        int nodeCount = mtTree.getNodeCount();
        Node node;
        for(int nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++){
            node = mtTree.getNode(nodeIndex);
            if(node.isLeaf()){
                nodeTypes[nodeIndex] = ((MultiTypeNodeVolz)node).getNodeType();
            }
        }
    }

    public void log(int nSample, PrintStream out) {
        double prob=lineageProbsList.get(lineageProbsList.size()-1)[0];
        if (prob==prob){
            //System.out.println("Set all demes called from log "+prob+"\n");
            out.print("tree STATE_" + nSample + " = ");
            out.print(toSortedNewickVolz(mtTree.getRoot(), new int[1])+";");
            loggable=true;
        }else{ loggable=false;}
    }

    /**
     * Determines the sequence of coalescence and sampling events
     * which make up the Volz tree.
     */
    public void updateEventSequence() {

        // Clean up previous list:
        eventList.clear();
        lineageCountList.clear();
        lineageCountSquareList.clear();
        lineageCountListAfter.clear();
        lineageCountSquareListAfter.clear();
        lineageProbsList.clear();
        Node rootNode = mtTree.getRoot();
        int nTypes= migrationModelVolz.getNDemes();

        // Initialise map of active nodes to active change indices:
        Map<Node, Integer> changeIdx = new HashMap<Node, Integer>();
        changeIdx.put(rootNode, -1);

        // Calculate event sequence:
        while (!changeIdx.isEmpty()) {

            SCEvent nextEvent = new SCEvent();
            nextEvent.time = Double.NEGATIVE_INFINITY;
            nextEvent.node = rootNode; // Initial assignment not significant

            // Determine next event
            for (Node node : changeIdx.keySet())
                if (changeIdx.get(node)<0) {
                    if (node.isLeaf()) {
                        // Next event is a sample
                        if (node.getHeight()>nextEvent.time) {
                            nextEvent.time = node.getHeight();
                            nextEvent.kind = SCEventKind.SAMPLE;
                            //nextEvent.type = nodeTypes[((MultiTypeNodeVolz) node).getNr()];//((MultiTypeNodeVolz)node).getNodeType();
                            nextEvent.type = ((MultiTypeNodeVolz)node).getNodeType();
                            nextEvent.node = node;
                        }
                    } else {
                        // Next event is a coalescence
                        if (node.getHeight()>nextEvent.time) {
                            nextEvent.time = node.getHeight();
                            nextEvent.kind = SCEventKind.COALESCE;
                            nextEvent.node = node;
                            nextEvent.type = nodeTypes[((MultiTypeNodeVolz) node).getNr()];
                        }
                    }
                }


            // Update active node list (changeIdx) and lineage count appropriately:
            switch (nextEvent.kind) {
                case COALESCE:
                    Node leftChild = nextEvent.node.getLeft();
                    Node rightChild = nextEvent.node.getRight();

                    changeIdx.remove(nextEvent.node);
                    changeIdx.put(leftChild, -1);
                    changeIdx.put(rightChild, -1);
                    break;

                case SAMPLE:
                    changeIdx.remove(nextEvent.node);
                    break;
            }

            // Add event to list:
            eventList.add(nextEvent);
        }




        // Reverse event and lineage count lists (order them from tips to root):
        eventList = Lists.reverse(eventList);




        // Initialise lineage count per colour array:
        SCEvent nextEvent = eventList.get(0);
        double coalTime=nextEvent.time;
        Double[] lineageCount = new Double[nTypes];
        for (int c = 0; c<nTypes; c++)
            if (nextEvent.type==c) {
                lineageCount[c]=1.0;
            }else{
                lineageCount[c]=0.0;
            }

        //Initialize first node
        MultiTypeNodeVolz firstNode = (MultiTypeNodeVolz) nextEvent.node;
        firstNode.setCacheIndex(0);
        setLikes(Arrays.copyOf(lineageCount, lineageCount.length),firstNode.getNr());
        //firstNode.setLikes(Arrays.copyOf(lineageCount, lineageCount.length));

        //Initialize square count
        Double[] lineageCountSquare = new Double[nTypes];
        for (int c = 0; c<nTypes; c++) lineageCountSquare[c]=lineageCount[c];
        lineageCountList.add(null);
        lineageCountSquareList.add(null);
        lineageCountListAfter.add(Arrays.copyOf(lineageCount, lineageCount.length));
        lineageCountSquareListAfter.add(Arrays.copyOf(lineageCount, lineageCount.length));
        //initialize cache of probabilities for lineages to be in demes
        List<Double[]> lineageProbs = new ArrayList<Double[]>();
        lineageProbs.add(Arrays.copyOf(lineageCount, lineageCount.length));
        lineageProbsList.add(null);
        //List to keep track of how many lineages are being cached on the same probability vector.
        List<Integer> numCached = new ArrayList<Integer>();
        numCached.add(1);
        //List to keep track of which types the cache represent, if they are samples just inserted
        List<Integer> typeCached = new ArrayList<Integer>();
        typeCached.add(nextEvent.type);

        //Count expected number of lineages in each deme, etc, going from bottom to top
        double[] pMatrix = new double[nTypes*nTypes];
        Double[] newLineageCount = new Double[nTypes];

        //boolean useJblas=false;
        if (migrationModelVolz.getJblas()==false){
            migrationModelVolz.getTransitionProbabilities(1.0, pMatrix);
            for (int i = 0; i < nTypes; i++) {
                double totFrom=0.0;
                for (int j = 0; j < nTypes; j++) {
                    totFrom+=pMatrix[(i*nTypes) + j];
                }
                if ((totFrom<0.9)||(totFrom>1.1)){
                    migrationModelVolz.setJblas(true);
                    //System.out.println("problems with beast exponentiation, using jblas");
                }
                //        	else{
                //        		System.out.println("beast exponentiation ok");
                //        	}
            }
        }


        for (int c = 1; c<eventList.size(); c++) {
            nextEvent = eventList.get(c);
            if ((nextEvent.time != coalTime)||(nextEvent.kind == SCEventKind.COALESCE)){
                double delta_t = nextEvent.time - coalTime;

                if (migrationModelVolz.getJblas()==false){
                    //BEAST2 exponentiation////////////////////////////////////////////////////////
                    migrationModelVolz.getTransitionProbabilities(delta_t, pMatrix);
                    //System.out.println("Using JBLAS\n");
                }else{
                    //use jblas to calculate pMatrix, the probability matrix of migration events
                    DoubleMatrix Qsupp = new DoubleMatrix(nTypes, nTypes);
                    for (int i = 0; i < nTypes; i++) {
                        Qsupp.put(i,i, 0.0);
                        for (int j = 0; j < nTypes; j++) {
                            if (i != j) {
                                Qsupp.put(i, j, migrationModelVolz.getRate(i, j)*delta_t);
                                Qsupp.put(i, i, Qsupp.get(i, i) - Qsupp.get(i, j));
                            }
                        }
                    }
                    DoubleMatrix Fsupp = MatrixFunctions.expm(Qsupp);
                    for (int t = 0; t<nTypes; t++) {
                        for (int t2 = 0; t2<nTypes; t2++) {
                            pMatrix[(t*nTypes) + t2]=Fsupp.get(t, t2);
                        }
                    }
                    //System.out.println("Using BEAST\n");
                }
                //System.out.println("Matrix exponentiation is:\n");
//        		for (int t = 0; t<pMatrix.length; t++) {
//        			System.out.println("Index "+t+" "+pMatrix[t]);
//        		}


                //find new expected counts of lineages in demes
                for (int t = 0; t<nTypes; t++) {
                    newLineageCount[t]=0.0;
                    for (int t2 = 0; t2<nTypes; t2++) {
                        newLineageCount[t]+=lineageCount[t2]*pMatrix[(t2*nTypes) + t];
                    }

                }
                for (int t = 0; t<nTypes; t++) lineageCount[t]=newLineageCount[t];
                lineageCountList.add(Arrays.copyOf(lineageCount, lineageCount.length));

                //find new expected squared counts of lineages in demes
                for (int t = 0; t<nTypes; t++) lineageCountSquare[t]=0.0;
                for (int ca = 0; ca<numCached.size(); ca++) {
                    if (numCached.get(ca)>0){
                        Double[] prob = lineageProbs.get(ca);
                        Double[] newProbs = new Double[nTypes];
                        for (int t = 0; t<nTypes; t++) {
                            double newProb=0.0;
                            for (int t2 = 0; t2<nTypes; t2++) {
                                newProb+=prob[t2]*pMatrix[(t2*nTypes) +t];
                            }
                            //System.out.println("Updated prob after lineage: "+newProb);
                            newProbs[t]=newProb;
                        }
                        lineageProbs.set(ca, Arrays.copyOf(newProbs, nTypes));
                        for (int t = 0; t<nTypes; t++) {
                            lineageCountSquare[t]+=newProbs[t]*newProbs[t]*numCached.get(ca);
                        }
                    }
                }
                lineageCountSquareList.add(Arrays.copyOf(lineageCountSquare, lineageCountSquare.length));

                //update counts after the event (not used for present coalescing rates)
                switch (nextEvent.kind) {
                    case COALESCE:
                        MultiTypeNodeVolz leftChild = (MultiTypeNodeVolz) nextEvent.node.getLeft();
                        MultiTypeNodeVolz rightChild = (MultiTypeNodeVolz) nextEvent.node.getRight();
                        int index1=leftChild.getCacheIndex();
                        int index2=rightChild.getCacheIndex();

                        //pass probability vectors right before coalescence for likelihood calculation
                        Double[] lineageProbsPair = new Double[2*nTypes];
                        for (int t = 0; t<nTypes; t++) lineageProbsPair[t]=lineageProbs.get(index1)[t];
                        for (int t = 0; t<nTypes; t++) lineageProbsPair[t+nTypes]=lineageProbs.get(index2)[t];
                        lineageProbsList.add(Arrays.copyOf(lineageProbsPair, 2*nTypes));

                        //calculate the probability vector for the new coalesced lineage, following the coalescent event
                        double tot=0.0;
                        for (int t = 0; t<nTypes; t++) tot+= (lineageProbs.get(index1)[t]*lineageProbs.get(index2)[t]/migrationModelVolz.getPopSize(t));
//                    System.out.println("Coalescence");
//                    for (int t = 0; t<nTypes; t++) {
//                    	System.out.println("Coalescence, index "+t+" has prob1 "+lineageProbs.get(index1)[t]+" prob2 "+lineageProbs.get(index2)[t]+" popsize "+migrationModelVolz.getPopSize(t));
//                    }
                        Double[] newProb = new Double[nTypes];
                        for (int t = 0; t<nTypes; t++) newProb[t]=(lineageProbs.get(index1)[t]*lineageProbs.get(index2)[t]/migrationModelVolz.getPopSize(t))/tot;

                        lineageProbs.add(Arrays.copyOf(newProb, nTypes));
                        setLikes(Arrays.copyOf(newProb, nTypes), nextEvent.node.getNr());



                        MultiTypeNodeVolz coalesced = (MultiTypeNodeVolz) nextEvent.node;
                        coalesced.setCacheIndex(lineageProbs.size()-1);
                        numCached.add(1);
                        numCached.set(index1, numCached.get(index1)-1);
                        numCached.set(index2, numCached.get(index2)-1);
                        for (int t = 0; t<typeCached.size(); t++) typeCached.set(t,-1);
                        typeCached.add(-1);
                        //set node location according to bottom-to-top likelihood calculation- works well only for the root!
                        //double u = Randomizer.nextDouble();
                        //double sumProb=0.0;
                        //int newNodeType=-1;
                        //for (int t = 0; t<nTypes; t++) {
                        //	sumProb=sumProb+newProb[t];
                        //	if (sumProb>= u){
                        //		newNodeType=t;
                        //	}
                        //}
                        //coalesced.setNodeType(newNodeType);
                        //nodeTypes[coalesced.getNr()]=newNodeType;


                        //Final update of counts, following the coalescent event
                        for (int t = 0; t<nTypes; t++)
                            lineageCount[t]=lineageCount[t]-(lineageProbs.get(index1)[t]+lineageProbs.get(index2)[t])+newProb[t];
                        for (int t = 0; t<nTypes; t++)
                            lineageCountSquare[t]=lineageCountSquare[t]-(lineageProbs.get(index1)[t]*lineageProbs.get(index1)[t]+lineageProbs.get(index2)[t]*lineageProbs.get(index2)[t])+newProb[t]*newProb[t];

                        ///put probability distribution of root!!!
                        if (c==(eventList.size()-1)){
                            lineageProbsList.add(Arrays.copyOf(newProb, nTypes));
//                    	System.out.println("Root\n");
//                    	for (int i = 0; i<nTypes; i++) {
//                			System.out.println("Root Probability at index "+i+" = "+lineageProbsList.get(lineageProbsList.size()-1)[i]+"\n");
//                    	}
                        }



                        break;

                    case SAMPLE:
                        //Adding a sample at a new time point
                        numCached.add(1);
                        for (int t = 0; t<typeCached.size(); t++) typeCached.set(t,-1);
                        typeCached.add(nextEvent.type);
                        lineageProbsList.add(null);
                        lineageCount[nextEvent.type]+=1.0;
                        lineageCountSquare[nextEvent.type]+=1.0;
                        //System.out.println(nextEvent.node.getNr()+" "+nextEvent.type);
                        Double[] probs = new Double[nTypes];
                        for (int t = 0; t<nTypes; t++)
                            if (nextEvent.type==t) {
                                probs[t]=1.0;
                            }else{
                                probs[t]=0.0;
                            }
                        lineageProbs.add(Arrays.copyOf(probs, nTypes));
                        setLikes(Arrays.copyOf(probs, nTypes), nextEvent.node.getNr());
                        //((MultiTypeNodeVolz) nextEvent.node).setLikes(Arrays.copyOf(probs, nTypes));
                        MultiTypeNodeVolz sampled = (MultiTypeNodeVolz) nextEvent.node;
                        sampled.setCacheIndex(lineageProbs.size()-1);
                        break;
                }


            }else{     //new sampling at the same time point
                lineageCountList.add(Arrays.copyOf(lineageCount, lineageCount.length));
                lineageCountSquareList.add(Arrays.copyOf(lineageCountSquare, lineageCountSquare.length));
                lineageProbsList.add(null);
                int type = nextEvent.type;
                int found=0;
                for (int t = 0; t<typeCached.size(); t++) {
                    if (type==typeCached.get(t)){//re-using the same cached probability values
                        found=1;
                        numCached.set(t,numCached.get(t)+1);
                        MultiTypeNodeVolz sampled = (MultiTypeNodeVolz) nextEvent.node;
                        sampled.setCacheIndex(t);
                        break;
                    }
                }
                Double[] probs = new Double[nTypes];
                for (int t = 0; t<nTypes; t++)
                    if (type==t) {
                        probs[t]=1.0;
                    }else{
                        probs[t]=0.0;
                    }
                setLikes(Arrays.copyOf(probs, nTypes), nextEvent.node.getNr());
                //((MultiTypeNodeVolz) nextEvent.node).setLikes(Arrays.copyOf(probs, nTypes));
                if (found==0){         // a new leaf of a new location is added
                    typeCached.add(type);
                    numCached.add(1);
                    lineageProbs.add(Arrays.copyOf(probs, nTypes));
                    MultiTypeNodeVolz sampled = (MultiTypeNodeVolz) nextEvent.node;
                    sampled.setCacheIndex(lineageProbs.size()-1);
                }
                //System.out.println(type);
                lineageCount[type]+=1.0;
                lineageCountSquare[type]+=1.0;

            }

            lineageCountListAfter.add(Arrays.copyOf(lineageCount, lineageCount.length));
            lineageCountSquareListAfter.add(Arrays.copyOf(lineageCountSquare, lineageCountSquare.length));
            coalTime=nextEvent.time;

        }


        //if(sampleMig) migrationCounts = new int[mtTree.getNTypes()*mtTree.getNTypes()];
        migrationCounts = new int[migrationModelVolz.getNDemes()*migrationModelVolz.getNDemes()];
        //pick location for all internal nodes and sample migration events if requested
        double prob=lineageProbsList.get(lineageProbsList.size()-1)[0];
        if (prob==prob){
            //System.out.println("Set all demes called from end of update sequence events "+prob+"\n");
            loggable=true;
            //setAllDemes();
        }else{ loggable=false;}
        //setAllDemes();
        //System.out.println("Total Migration type counts: "+Arrays.toString(migrationCounts));
    }
}
