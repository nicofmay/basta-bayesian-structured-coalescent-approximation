package basta.operators;

import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.inference.Operator;
import beast.base.inference.parameter.IntegerParameter;
import beast.base.inference.parameter.Parameter;
import beast.base.inference.parameter.RealParameter;
import beast.base.util.Randomizer;

@Description("Assign one or more parameter values to a uniformly selected value in its range.")
public class ShortRangeUniformOperator extends Operator {
    public Input<Parameter<?>> parameterInput = new Input<Parameter<?>>("parameter", "a real or integer parameter to sample individual values for", Validate.REQUIRED, Parameter.class);
    public Input<Integer> howManyInput = new Input<Integer>("howMany", "number of items to sample, default 1, must be less than the dimension of the parameter", 1);
    public Input<Double> rangeInput = new Input<Double>("range", "range within which uniformly sample, for example, if range is 0.2, a maximum increase and decrease of 20% is allowed.", 0.5);

    int howMany;
    double range;
    Parameter<?> parameter;
    double fLower, fUpper;
    int iLower, iUpper;

    @Override
    public void initAndValidate() throws IllegalArgumentException {
        parameter = parameterInput.get();
        range=rangeInput.get();
        if (parameter instanceof RealParameter) {
            fLower = (Double) parameter.getLower();
            fUpper = (Double) parameter.getUpper();
        } else if (parameter instanceof IntegerParameter) {
            iLower = (Integer) parameter.getLower();
            iUpper = (Integer) parameter.getUpper();
        } else {
            throw new IllegalArgumentException("parameter should be a RealParameter or IntergerParameter, not " + parameter.getClass().getName());
        }

        howMany = howManyInput.get();
        if (howMany > parameter.getDimension()) {
            throw new IllegalArgumentException("howMany it too large: must be less than the dimension of the parameter");
        }
    }

    @Override
    public double proposal() {
        for (int n = 0; n < howMany; ++n) {
            // do not worry about duplication, does not matter
            int index = Randomizer.nextInt(parameter.getDimension());

            if (parameter instanceof IntegerParameter) {
            	int presentValue=((IntegerParameter) parameter).getValue(index);
            	int upRange=(int)Math.ceil(presentValue*(1.0+range));
            	int downRange=(int)Math.floor(presentValue*(1.0-range));
            	downRange=Math.max(downRange, iLower);
            	upRange=Math.min(upRange, iUpper);
                //int newValue = Randomizer.nextInt(iUpper - iLower + 1) + iLower; // from 0 to n-1, n must > 0,
            	int newValue = Randomizer.nextInt(upRange - downRange)+ downRange;
            	if (newValue==presentValue) newValue++;
                //int newValue = Randomizer.nextInt(upRange - downRange + 1) + downRange; // from 0 to n-1, n must > 0,
                ((IntegerParameter) parameter).setValue(index, newValue);
            } else {
            	double presentValue=((IntegerParameter) parameter).getValue(index);
            	double upRange=presentValue*(1.0+range);
            	double downRange=presentValue*(1.0-range);
            	downRange=Math.max(downRange, iLower);
            	upRange=Math.min(upRange, iUpper);
                double newValue = Randomizer.nextDouble() * (upRange - downRange) + downRange;
                ((RealParameter) parameter).setValue(index, newValue);
            }

        }

        return 0.0;
    }

}
