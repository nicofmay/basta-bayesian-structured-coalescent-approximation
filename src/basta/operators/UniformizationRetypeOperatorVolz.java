/*
 * Copyright (C) 2014 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package basta.operators;

import basta.evolution.tree.MigrationModelVolz;
import basta.evolution.tree.MultiTypeTreeFromNewickVolz;
import beast.base.core.Input;
import beast.base.inference.State;
import beast.base.inference.parameter.RealParameter;

import java.io.PrintStream;
//import java.util.Arrays;
//import org.jblas.MatrixFunctions;

/**
 * Abstract class of operators on MultiTypeTrees which use the Fearnhead-Sherlock
 * uniformization/forward-backward algorithm for branch retyping.
 *
 * @author Nicola De Maio
 */
public abstract class UniformizationRetypeOperatorVolz extends MultiTypeTreeOperatorVolz {
    
    public Input<MigrationModelVolz> migrationModelInput = new Input<MigrationModelVolz>(
            "migrationModel",
            "Migration model for proposal distribution", Input.Validate.REQUIRED);
    
    public Input<Boolean> useSymmetrizedRatesInput = new Input<Boolean>(
            "useSymmetrizedRates",
            "Use symmetrized rate matrix to propose migration paths.", false);


    /**
     * Main method for debugging.
     * 
     * @param args 
     */
    public static void main(String[] args) throws Exception {
       
        // Generate an ensemble of paths along a branch of a tree.
        
        // Assemble initial MultiTypeTree
        String newickStr =
                "((1[deme='3']:50)[deme='1']:150,2[deme='1']:200)[deme='1']:0;";
        
        MultiTypeTreeFromNewickVolz mtTree = new MultiTypeTreeFromNewickVolz();
        mtTree.initByName(
                "newick", newickStr,
                "typeLabel", "deme",
                "nTypes", 4);
        
        // Assemble migration model:
        RealParameter rateMatrix = new RealParameter(
                "0.20 0.02 0.03 0.04 "
                + "0.05 0.06 0.07 0.08 "
                + "0.09 0.10 0.11 0.12");
        RealParameter popSizes = new RealParameter("7.0 7.0 7.0 7.0");
        MigrationModelVolz migModel = new MigrationModelVolz();
        migModel.initByName(
                "rateMatrix", rateMatrix,
                "popSizes", popSizes);
        
        // Set up state:
        State state = new State();
        state.initByName("stateNode", mtTree);
        
        PrintStream outfile = new PrintStream("counts.txt");
        outfile.print("totalCounts");
        for (int c=0; c<mtTree.getNTypes(); c++)
            outfile.print(" counts" + c);
        outfile.println();
        
        outfile.close();
    }

}
