/*
 * Copyright (C) 2014 Nicola De Maio <nicola.de.maio.85@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package basta.operators;

import basta.evolution.tree.MigrationModelVolz;
import beast.base.core.Description;
import beast.base.util.Randomizer;

/**
 *
 * @author Nicola De Maio
 */
@Description("Swaps two types on a tree, including migration matrix elements "
        + "and population sizes.")
public class TypeSwapVolz extends UniformizationRetypeOperatorVolz {
    
    @Override
    public void initAndValidate() { }
    
    @Override
    public double proposal() {
        
        mtTree = multiTypeTreeInput.get();
        MigrationModelVolz migModel = migrationModelInput.get();

        double logHR = 0.0;
        
        // Select types to swap:
        int typeA = Randomizer.nextInt(mtTree.getNTypes());
        int typeB;
        do {
            typeB = Randomizer.nextInt(mtTree.getNTypes());
        } while (typeB == typeA);

        // Swap involved rows and columns of the migration matrix:
        for (int i=0; i<mtTree.getNTypes(); i++) {
            if (i == typeA || i == typeB)
                continue;

            // Swap cols:
            double oldA = migModel.getRate(i, typeA);
            double oldB = migModel.getRate(i, typeB);
            migModel.setRate(i, typeA, oldB);
            migModel.setRate(i, typeB, oldA);
            
            // Swap rows:
            oldA = migModel.getRate(typeA, i);
            oldB = migModel.getRate(typeB, i);
            migModel.setRate(typeA, i, oldB);
            migModel.setRate(typeB, i, oldA);
        }
        
        double old1 = migModel.getRate(typeA, typeB);
        double old2 = migModel.getRate(typeB, typeA);
        migModel.setRate(typeB, typeA, old1);
        migModel.setRate(typeA, typeB, old2);
        
        // Swap population sizes:
        old1 = migModel.getPopSize(typeA);
        old2 = migModel.getPopSize(typeB);
        migModel.setPopSize(typeA, old2);
        migModel.setPopSize(typeB, old1);
        
        return logHR;
    }
    
}
