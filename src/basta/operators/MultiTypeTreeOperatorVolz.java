/*
 * Copyright (C) 2014 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package basta.operators;

import basta.evolution.tree.MultiTypeNodeVolz;
import basta.evolution.tree.MultiTypeTreeVolz;
import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.Tree;
import beast.base.inference.Operator;

/**
 * Abstract base class for all operators on Volz-Tree objects.
 *
 * @author Nicola De Maio
 */
@Description("This operator generates proposals for a coloured beast tree.")
public abstract class MultiTypeTreeOperatorVolz extends Operator {

    public Input<MultiTypeTreeVolz> multiTypeTreeInput = new Input<MultiTypeTreeVolz>(
            "multiTypeTree", "Multi-type tree on which to operate.",
            Validate.REQUIRED);
    
    protected MultiTypeTreeVolz mtTree;
    
    /* ***********************************************************************
     * The following two methods are copied verbatim from TreeOperator.
     */
    
    /**
     * Obtain the sister of node "child" having parent "parent".
     * 
     * @param parent the parent
     * @param child  the child that you want the sister of
     * @return the other child of the given parent.
     */
    protected Node getOtherChild(Node parent, Node child) {
        if (parent.getLeft().getNr() == child.getNr()) {
            return parent.getRight();
        } else {
            return parent.getLeft();
        }
    }

    /**
     * Replace node "child" with another node.
     *
     * @param node
     * @param child
     * @param replacement
     */
    public void replace(Node node, Node child, Node replacement) {
    	node.removeChild(child);
    	node.addChild(replacement);
        node.makeDirty(Tree.IS_FILTHY);
        replacement.makeDirty(Tree.IS_FILTHY);
    }
    
    /* **********************************************************************/

    /**
     * Disconnect edge <node,node.getParent()> by joining node's sister directly
     * to node's grandmother.
     *
     * @param node
     */
    public void disconnectBranch(Node node) {

        // Check argument validity:
        Node parent = node.getParent();
        if (node.isRoot() || parent.isRoot())
            throw new IllegalArgumentException("Illegal argument to "
                    + "disconnectBranch().");

        Node sister = getOtherChild(parent, node);

        // Implement topology change.
        replace(parent.getParent(), parent, sister);
        
        // Clear colour changes from parent:
        ((MultiTypeNodeVolz)parent).clearChanges();
        
        // Ensure BEAST knows to update affected likelihoods:
        parent.makeDirty(Tree.IS_FILTHY);
        sister.makeDirty(Tree.IS_FILTHY);
        node.makeDirty(Tree.IS_FILTHY);
    }

    /**
     * Disconnect node from root.
     *
     * @param node
     */
    public void disconnectBranchFromRoot(Node node) {

        // Check argument validity:
        if (node.isRoot() || !node.getParent().isRoot())
            throw new IllegalArgumentException("Illegal argument to"
                    + " disconnectBranchFromRoot().");

        // Implement topology change:
        Node parent = node.getParent();
        Node sister = getOtherChild(parent, node);
        sister.setParent(null);
        parent.removeChild(sister);
        //parent.getChildren().remove(sister);
        
        // Clear colour changes on new root:
        ((MultiTypeNodeVolz)sister).clearChanges();

        // Ensure BEAST knows to update affected likelihoods:
        parent.makeDirty(Tree.IS_FILTHY);
        sister.makeDirty(Tree.IS_FILTHY);
        node.makeDirty(Tree.IS_FILTHY);
    }

    /**
     * Creates a new branch between node and a new node at time destTime between
     * destBranchBase and its parent.
     *
     * @param node
     * @param destBranchBase
     * @param destTime
     */
    public void connectBranch(Node node,
            Node destBranchBase, double destTime) {

        // Check argument validity:
        if (node.isRoot() || destBranchBase.isRoot())
            throw new IllegalArgumentException("Illegal argument to "
                    + "connectBranch().");

        // Obtain existing parent of node and set new time:
        Node parent = node.getParent();
        parent.setHeight(destTime);
        
        MultiTypeNodeVolz mtParent = (MultiTypeNodeVolz)parent;
        mtParent.clearChanges();
        mtParent.setNodeType(-1);

        // Implement topology changes:
        replace(destBranchBase.getParent(), destBranchBase, parent);
        destBranchBase.setParent(parent);

        if (parent.getLeft() == node)
            parent.setRight(destBranchBase);
        else if (parent.getRight() == node)
            parent.setLeft(destBranchBase);

        // Ensure BEAST knows to update affected likelihoods:
        node.makeDirty(Tree.IS_FILTHY);
        parent.makeDirty(Tree.IS_FILTHY);
        destBranchBase.makeDirty(Tree.IS_FILTHY);
    }

    /**
     * Set up node's parent as the new root with a height of destTime, with
     * oldRoot as node's new sister.
     *
     * @param node
     * @param oldRoot
     * @param destTime
     */
    public void connectBranchToRoot(Node node, Node oldRoot, double destTime) {

        // Check argument validity:
        if (node.isRoot() || !oldRoot.isRoot())
            throw new IllegalArgumentException("Illegal argument "
                    + "to connectBranchToRoot().");

        // Obtain existing parent of node and set new time:
        Node newRoot = node.getParent();
        newRoot.setHeight(destTime);

        // Implement topology changes:

        newRoot.setParent(null);

        if (newRoot.getLeft() == node)
            newRoot.setRight(oldRoot);
        else if (newRoot.getRight() == node)
            newRoot.setLeft(oldRoot);

        oldRoot.setParent(newRoot);

        // Ensure BEAST knows to recalculate affected likelihood:
        newRoot.makeDirty(Tree.IS_FILTHY);
        oldRoot.makeDirty(Tree.IS_FILTHY);
        node.makeDirty(Tree.IS_FILTHY);
    }

}