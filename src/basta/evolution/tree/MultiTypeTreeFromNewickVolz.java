package basta.evolution.tree;


import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.evolution.tree.Tree;
import beast.base.evolution.tree.TreeParser;
import beast.base.inference.StateNode;
import beast.base.inference.StateNodeInitialiser;

import java.util.List;

/**
 * @author Nicola De Maio
 */

@Description("Class to initialize a VolzTree from single child newick tree with type metadata")
public class MultiTypeTreeFromNewickVolz extends MultiTypeTreeVolz implements StateNodeInitialiser {

    public Input<String> newickStringInput = new Input<String>("newick",
            "Tree in Newick format.", Validate.REQUIRED);

    public Input<Boolean> adjustTipHeightsInput = new Input<Boolean>("adjustTipHeights",
            "Adjust tip heights in tree? Default true.", true);

    @Override
    public void initAndValidate() throws IllegalArgumentException {
        
        super.initAndValidate();
        
        //MultiTypeTreeFromNewickVolz mtTree = new MultiTypeTreeFromNewickVolz();
        //this.initByName(
        //        "newick", newickStringInput.get(),
        //        "typeLabel", "type",
        //        "nTypes", 2);
        
        try {
        //System.out.println("new TreeParser(); ");
        TreeParser parser = new TreeParser();
        //System.out.println("Starting parser.initByName ");
        parser.initByName(
                "IsLabelledNewick", true,
                "adjustTipHeights", adjustTipHeightsInput.get(),
                "singlechild", true,
                "newick", newickStringInput.get());
        Tree tree = parser;
        //System.out.println("First step in initialization done. Now initFromNormalTree");
        initFromNormalTree(tree, true);
        //System.out.println("initFromNormalTree done.");
        //List<TraitSet> a= new ArrayList<TraitSet>();
        //this.processTraits(a);
        //System.out.println("processed traits (necessary?).");
        //for (int i = 0; i<getLeafNodeCount(); i++)
        //    ((MultiTypeNodeVolz)getNode(i)).setNodeType(
        //            getTypeList().indexOf(typeTraitSet.getStringValue(this.getTaxonId(i))));
        for (int i = 0; i<getLeafNodeCount(); i++) {
        	//System.out.println("i="+i+" getNode(i).labelNr="+getNode(i).labelNr+" this.getTaxonId((MultiTypeNodeVolz)getNode(i))="+this.getTaxonId((MultiTypeNodeVolz)getNode(i)));
        	//System.out.println(" getTypeList().get(0)="+getTypeList().get(0)+" getTypeList().get(1)="+getTypeList().get(1));
        	//System.out.println(" typeTraitSet.getStringValue(this.getTaxonId((MultiTypeNodeVolz)getNode(i)))="+typeTraitSet.getStringValue(this.getTaxonId((MultiTypeNodeVolz)getNode(i))));
        	//System.out.println(" getTypeList().indexOf(typeTraitSet.getStringValue(this.getTaxonId((MultiTypeNodeVolz)getNode(i))))="+getTypeList().indexOf(typeTraitSet.getStringValue(this.getTaxonId((MultiTypeNodeVolz)getNode(i)))));
        	((MultiTypeNodeVolz)getNode(i)).setNodeType(getTypeList().indexOf(typeTraitSet.getStringValue(this.getTaxonId((MultiTypeNodeVolz)getNode(i)))));
        }
        //System.out.println("Set leaf types (useful?).");
        
        } catch (Exception ex) {
            System.out.println("caught error in initialzing tree from Newick format, probably initial tree had no geographic states, but it does not matter.");
        }
    }

    @Override
    public void initStateNodes() { }

    @Override
    public void getInitialisedStateNodes(List<StateNode> stateNodeList) {
        stateNodeList.add(this);
    }
}
