package basta.evolution.tree;

//import java.text.SimpleDateFormat;
//import java.util.Date;




//import java.util.Arrays;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;

import beast.base.core.Description;
import beast.base.evolution.tree.TraitSet;


@Description("A trait set with a few added functions.")
public class TraitSetWithLimits extends TraitSet {
	
	/**
     * double representation of taxa value *
     */
    //double[] values;
    //double minValue;
    //double maxValue;
    
    //Map<String, Integer> map;
    
    public double getValue(String taxonName) {
        if (values == null || map == null) {
            return 0;
        }
        //Log.trace.println("Trait " + taxonName + " => " + values[map.get(taxonName)]);
        return values[map.get(taxonName)];
    }
    
    public double getMinValue() {
        return minValue;
    }
    
    public double getMaxValue() {
        return maxValue;
    }
    
    public String[] getStringValues() {
        return taxonValues;
    }
    
} // class TraitSet
