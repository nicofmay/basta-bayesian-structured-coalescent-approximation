package basta.core;

import beast.base.core.Function;
import beast.base.core.Input;
import beast.base.core.Loggable;
import beast.base.inference.CalculationNode;
import beast.base.inference.parameter.RealParameter;

import java.io.PrintStream;

/**
 * Created by jessiewu on 22/02/16.
 */
public class SubsetRealParameter extends CalculationNode implements Function, Loggable {
    public Input<RealParameter> parameterInput = new Input<RealParameter>(
            "parameter",
            "The parameter whose subset is of interest.",
            Input.Validate.REQUIRED
    );

    public Input<String> indexInput = new Input<String>(
            "index",
            "A set indexes corresponding to the elements of interest in the parameter object.",
            Input.Validate.REQUIRED
    );

    private RealParameter parameter;
    private int[] index;
    public void initAndValidate(){
        parameter = parameterInput.get();
        String[] indexElt = indexInput.get().trim().split("\\s+");
        int paramLength = parameter.getDimension();
        index = new int[indexElt.length];
        for(int i = 0; i < index.length; i++){
            index[i] = Integer.parseInt(indexElt[i]);
            if(index[i] >= paramLength){
                throw new RuntimeException("Index out of bounds: "+index[0]+"\n" +
                        "Parameter dimension = "+paramLength+".");
            }
        }
    }

    public int getDimension(){
        return index.length;
    }

    public double getArrayValue(){
        return parameter.getValue(index[0]);
    }

    public double getArrayValue(int iDim){
        return parameter.getValue(index[iDim]);
    }

    public void init(PrintStream out){
        final int nValues = getDimension();
        if (nValues == 1) {
            out.print(getID() + "\t");
        } else {
            for (int iValue = 0; iValue < nValues; iValue++) {
                out.print(getID() + (iValue + 1) + "\t");
            }
        }
    }

    /**
     * log this sample for current state to PrintStream,
     * e.g. value of a parameter, list of parameters or Newick tree
     *
     * @param nSample chain sample number
     * @param out     log stream
     */
    public void log(long nSample, PrintStream out){
        final int nValues = getDimension();
        if (nValues == 1) {
            out.print(getArrayValue() + "\t");
        } else {
            for (int iValue = 0; iValue < nValues; iValue++) {
                out.print(getArrayValue(iValue) + "\t");
            }
        }
    }

    /**
     * close log. An end of log message can be left (as in End; for Nexus trees)
     *
     * @param out log stream
     */
    public void close(PrintStream out){

    }
}

