package basta.util;

import basta.distributions.StructuredCoalescentTreeDensityVolz;
import basta.evolution.tree.MigrationModelVolz;
import basta.evolution.tree.MultiTypeNodeVolz;
import basta.evolution.tree.MultiTypeTreeFromNewickVolz;
import basta.evolution.tree.MultiTypeTreeVolz;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.Tree;
import beast.base.inference.parameter.RealParameter;
import beast.base.parser.NexusParser;

import java.io.*;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jessiewu on 05/10/16.
 */
public class SampleDemeForBASTA {
    public static void main(String[] args){

        String logFile = args[0];
        String treeFile = args[1];

        String migRateNamesFile = args[2];
        String popSizeNamesFile = args[3];
        String demesFile = args[4];



        //String[] migRateNames = new String[]{"rateMatrix"};
        //String[] popSizeNames = new String[]{"popSizes1", "popSizes2", "popSizes3", "popSizes4", "popSizes5","popSizes6", "popSizes7", "popSizes8"};
        //String[] demes = new String[]{"Brazil_North", "BrazilNortheast", "Brazil_Southeast", "Caribbean", "CentralAmerica", "Oceania_Polynesia", "SouthAmerica", "SoutheastAsia"};
        //String[] demes = new String[]{"Brazil_North", "BrazilNortheast", "Brazil_Southeast", "Caribbean", "CentralAmerica", "Polynesia", "SouthAmerica", "SouthEastAsia"};
        String outputLogFile = args[5];
        String outputTreeFile = args[6];
        String id=args[7];

        try{

            BufferedReader migRateNamesReader = new BufferedReader(new FileReader(migRateNamesFile));
            String[] migRateNames = migRateNamesReader.readLine().split("\t");

            BufferedReader popSizeNamesReader = new BufferedReader(new FileReader(popSizeNamesFile));
            String[] popSizeNames = popSizeNamesReader.readLine().split("\t");


            BufferedReader demesReader = new BufferedReader(new FileReader(demesFile));
            String[] demes = demesReader.readLine().split("\t");





            BufferedReader logReader = new BufferedReader(new FileReader(logFile));
            //Read in the headers
            String logLine = logReader.readLine();
            String[] headers = logLine.split("\t");



            int[] migRateColIndexes = getColIndex(headers, migRateNames);
            int[] popSizeColIndexes = getColIndex(headers, popSizeNames);
            Double[] migRateValues = new Double[migRateColIndexes.length];
            Double[] popSizeValues = new Double[popSizeColIndexes.length];
            String[] log;
            MultiTypeTreeVolz mTree;
            MigrationModelVolz migModel;

            int[] migCounts;

            //Read in trees
            NexusParser nexusParser = new NexusParser();
            nexusParser.parseFile(new File(treeFile));

            //Create a taxon-id table
            Tree tree = nexusParser.trees.get(0);
            HashMap<String, Integer> map = new HashMap<String, Integer>();
            List<Node> leaves = tree.getExternalNodes();
            for(Node leaf: leaves){
                map.put(tree.getTaxonId(leaf), leaf.getNr());
                System.out.println(tree.getTaxonId(leaf)+" "+ (leaf.getNr()+1));
            }








            /*Tree tree1 = nexusParser.trees.get(0);
            Tree tree2 = nexusParser.trees.get(1);
            tree1.getTaxonId(tree1.getNode(25));
            tree2.getTaxonId(tree2.getNode(25));
            System.out.println("id 25: "+tree1.getTaxonId(tree1.getNode(25))+" "+tree2.getTaxonId(tree2.getNode(25)));



            String newickTree1 = tree1.getRoot().toNewick();
            String newickTree2 = tree2.getRoot().toNewick();

            newickTree1 = newickTree1.replace("&&", "");
            newickTree1 = newickTree1.replace("location=", "type=");

            newickTree2 = newickTree2.replace("&&", "");
            newickTree2 = newickTree2.replace("location=", "type=");

            for(int i = 0; i < demes.length; i++){
                newickTree1 = newickTree1.replaceAll("type="+demes[i], "type=" + (double) i);
                newickTree2 = newickTree2.replaceAll("type="+demes[i], "type=" + (double) i);
            }

            MultiTypeTreeVolz mTree1 = createSCMultiTypeTreeVolz(newickTree1);
            MultiTypeTreeVolz mTree2 = createSCMultiTypeTreeVolz(newickTree2);




            if(25 == 25){
                throw new RuntimeException("");
            }*/





            /*for(int i = 0; i < tree.getNodeCount(); i++){
                System.out.println(tree.getNode(i).getHeight());
            }*/


            String newickTree;
            /*String newickTree = tree.getRoot().toNewick();
            newickTree = newickTree.replace("&&", "");
            newickTree = newickTree.replace("location=", "type=");

            System.out.println(newickTree);


            for(int i = 0; i < demes.length; i++){
                String temp = "type=" + demes[i];
                newickTree = newickTree.replaceAll(temp, "type=" + (double) i);
            }
            System.out.println(newickTree);

            mTree = createSCMultiTypeTreeVolz(newickTree);
            System.out.println("node counts: "+mTree.getNodeCount()+" "+mTree.getRoot().getHeight());
            for(int i = 0; i < mTree.getNodeCount(); i++){
                System.out.println(i + " " + mTree.getNode(i).getHeight());
            }*/

            //Create output streams
            PrintWriter writer = new PrintWriter(outputLogFile);
            PrintStream treeOutput = new PrintStream(outputTreeFile);
            init(treeOutput, tree);



            //BufferedReader treeReader = new BufferedReader(new FileReader(treeFile));

            //System.out.println(logLine);

            //Write the header of the log file for migration counts
            writer.print("Sample"+"\t"+"treePrior"+"\t");
            for (int index1 = 0; index1 < demes.length; index1++){
                for (int index2 = 0; index2 < demes.length; index2++){
                    if (index1 != index2){
                        writer.print("treePrior.Count"+index1+"to"+index2+"\t");
                    }
                }
            }
            writer.println();





            for(int treeIndex = 0; treeIndex < nexusParser.trees.size(); treeIndex++){
                //if(treeLine.contains("tree STATE")){
                //System.out.println(" treeIndex: "+ treeIndex);
                logLine = logReader.readLine();
                log = logLine.split("\t");
                writer.print(log[0].trim()+"\t");

                //Get migration rate
                getValues(log, migRateColIndexes, migRateValues);
                getValues(log, popSizeColIndexes, popSizeValues);

                /*    System.out.print("migRate: ");
                    for(int i = 0; i < migRateValues.length; i++){
                        System.out.print(migRateColIndexes[i]+ " ");
                    }
                    System.out.println();

                    System.out.print("popSize: ");
                    for(int i = 0; i < popSizeValues.length; i++){
                        System.out.print(popSizeColIndexes[i]+ " ");
                    }
                    System.out.println();*/

                //Get tree of posterior sample 'treeIndex'
                tree = nexusParser.trees.get(treeIndex);
                newickTree = tree.getRoot().toNewick();

                //Replace some keywords in the prefixes
                newickTree = newickTree.replace("&&", "");
                newickTree = newickTree.replace("location=", "type=");

                for(int i = 0; i < demes.length; i++){
                    newickTree = newickTree.replaceAll("type="+demes[i], "type=" + (double) i);
                }
                //System.out.println(newickTree);

                //Set up the tree
                mTree = createSCMultiTypeTreeVolz(newickTree);

                //Set up the migration model
                migModel = createMigrationModelVolz(migRateValues, popSizeValues);
                migModel.setJblas(true);
                migCounts = new int[migModel.getNDemes()*migModel.getNDemes()];

                //Set up the likelihood
                StructuredCoalescentTreeDensityVolz lik = createStructuredCoalescentTreeDensityVolz(mTree, migModel);

                double logP = lik.calculateLogP();
                writer.print(logP+"\t");

                //Sample the internal demes and calculate the migration counts
                SampleDemes sample = new SampleDemes();
                sample.initByName(
                        "migrationModelVolz", migModel,
                        "multiTypeTreeVolz", mTree,
                        "bastaLik", lik
                );

                sample.setAllDemes();
                sample.getMigrationCounts(migCounts);

                //Print out the migration counts
                for(int i = 0; i < migCounts.length; i++){
                    writer.print(migCounts[i]+"\t");
                }

                for (int index1 = 0; index1 < demes.length; index1++){
                    for (int index2 = 0; index2 < demes.length; index2++){
                        if (index1 != index2){
                            writer.print(migCounts[index1*demes.length + index2]+"\t");
                        }
                    }
                }
                writer.println();

                //
                log(Integer.parseInt(log[0].trim()), treeOutput, mTree, id, demes, map);



            }
            close(treeOutput);


            logReader.close();
            writer.close();
            treeOutput.close();

            migRateNamesReader.close();
            popSizeNamesReader.close();


        }catch(Exception e){
            throw new RuntimeException(e);
        }


    }

    public static String getTreeSampleNumber(String treeLine, String marker1, String marker2){
        int index1 = treeLine.indexOf(marker1)+marker1.length();
        int index2 = treeLine.indexOf(marker2);
        return treeLine.substring(index1, index2);
    }

    public static void getValues(String[] log, int[] colIndexes, Double[] values){
        for(int i = 0; i < colIndexes.length; i++){
            values[i] = Double.parseDouble(log[colIndexes[i]]);
        }

    }

    public static int[] getColIndex(String[] headers, String[] names){
        int[] indexes = new int[names.length];
        for(int i = 0; i < names.length; i++){
            indexes[i] = -1;
            System.out.println(names[i]);
            for(int j = 0; j < headers.length; j++){
                if(headers[j].trim().equals(names[i].trim())){
                    System.out.println("hi?");
                    indexes[i] = j;
                    break;
                }
            }
        }
        for(int i = 0; i < indexes.length; i++){
            System.out.println(indexes[i]);
        }
        return indexes;
    }

    public static MultiTypeTreeVolz createSCMultiTypeTreeVolz(String newickTree){

        try{
            MultiTypeTreeFromNewickVolz mtTree = new MultiTypeTreeFromNewickVolz();
            mtTree.initByName(
                    "newick", newickTree,
                    "typeLabel", "type",
                    "adjustTipHeights", false
            );

            return mtTree;

        }catch(Exception e){
            throw new RuntimeException(e);
        }

    }




    public static MigrationModelVolz createMigrationModelVolz(Double[] migRatesVals, Double[] popSizesVals){
        try{
            RealParameter migRates = new RealParameter(migRatesVals);
            RealParameter popSizes = new RealParameter(popSizesVals);
            MigrationModelVolz migrationModel = new MigrationModelVolz();
            migrationModel.initByName(
                    "rateMatrix", migRates,
                    "popSizes", popSizes
            );
            return migrationModel;

        }catch (Exception e){
            throw new RuntimeException(e);
        }

    }


    public static StructuredCoalescentTreeDensityVolz createStructuredCoalescentTreeDensityVolz(
            MultiTypeTreeVolz mTree, MigrationModelVolz migModel){
        try{

            //<multiTypeTreeVolz idref="tree"/>
            //<migrationModelVolz idref="migModel"/>

            StructuredCoalescentTreeDensityVolz lik = new StructuredCoalescentTreeDensityVolz();
            lik.initByName(
                    "multiTypeTreeVolz", mTree,
                    "migrationModelVolz", migModel
            );
            return lik;

        }catch (Exception e){
            throw new RuntimeException(e);
        }

    }

    public static void init(PrintStream out, Tree tree) throws Exception {
        tree.init(out);
        out.print("\n");
    }

    /** Print tree with demes represented as names instead of numbers **/
    public static void log(int nSample, PrintStream out, MultiTypeTreeVolz mtTree, String id, String[] demes, HashMap<String, Integer> map) {
        out.print("tree STATE_" + nSample + " = ");
        out.print(toSortedNewickVolz(mtTree.getRoot(), new int[1], id, mtTree, demes, map)+";");
        out.print("\n");

    }

    public static String toSortedNewickVolz(Node node, int[] iMaxNodeInClade, String id, MultiTypeTreeVolz mtTree, String[] demes, HashMap<String, Integer> map) {
        StringBuilder buf = new StringBuilder();
        if (node.getLeft() != null) {
            buf.append("(");
            String sChild1 = toSortedNewickVolz(node.getLeft(), iMaxNodeInClade, id, mtTree, demes, map);
            int iChild1 = iMaxNodeInClade[0];
            if (node.getRight() != null) {
                String sChild2 = toSortedNewickVolz(node.getRight(), iMaxNodeInClade, id, mtTree, demes, map);
                int iChild2 = iMaxNodeInClade[0];
                if (iChild1 > iChild2) {
                    buf.append(sChild2);
                    buf.append(",");
                    buf.append(sChild1);
                } else {
                    buf.append(sChild1);
                    buf.append(",");
                    buf.append(sChild2);
                    iMaxNodeInClade[0] = iChild1;
                }
            } else {
                buf.append(sChild1);
            }
            buf.append(")");
            if (id != null) {
                if(node.isLeaf()){
                    buf.append(map.get(mtTree.getTaxonId(node))+1);
                }else {
                    buf.append(node.getNr() + 1);
                }
            }
        } else {
            iMaxNodeInClade[0] = node.getNr();
            //buf.append(node.getNr() + 1);
            if(node.isLeaf()){
                buf.append(""+(map.get(mtTree.getTaxonId(node))+1));
            }else {
                buf.append(node.getNr() + 1);
            }
        }

        //if (printMetaData) {
        //System.out.println("mtTree.getTypeList(): "+migrationModelVolz.getNDemes());
        //buf.append(("[&location=" + mtTree.getTypeList().get(((MultiTypeNodeVolz) node).getNodeType())+"]"));
        buf.append(("[&location=" + demes[((MultiTypeNodeVolz) node).getNodeType()]+"]"));
        //if(node.isLeaf()&&node.getNr()==25) {
        //    System.out.println(mtTree.getTaxonId(node) + " " + ((MultiTypeNodeVolz) node).getNodeType());
       // }
        //}
        buf.append(":").append(node.getLength());

        return buf.toString();
    }

    public static void close(PrintStream out) {

        out.print("End;");

    }


}
