/*
 * Copyright (C) 2015 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package basta.util;

//import beast.evolution.tree.MigrationModelVolz;

import basta.distributions.StructuredCoalescentTreeDensityVolz;
import beast.base.core.Function;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.core.Loggable;
import beast.base.inference.CalculationNode;

import java.io.PrintStream;

/**
 * Special logger for constructing unit tests on multi type tree operator
 * combinations.
 *
 * @author Nicola De Maio
 */
public class MigrationCountsLoggerVolz extends CalculationNode implements Loggable, Function {
    
    public Input<StructuredCoalescentTreeDensityVolz> DensityInput = new Input<StructuredCoalescentTreeDensityVolz>(
            "density",
            "tree-migration model pair whose migration counts to log.",
            Validate.REQUIRED);
    
    StructuredCoalescentTreeDensityVolz density;
    
    //List<Double> counts = new ArrayList();
    int [] counts;
    //double heightMean, heightVar, heightESS;
    
    @Override
    public void initAndValidate() { }
    
    @Override
    public void init(PrintStream out) throws IllegalArgumentException {
    	density = DensityInput.get();
    	int n=density.getNDemes();
        for (int i1=0;i1<n;i1++){
        	for (int i2=0;i2<n;i2++){
        		if (i1!=i2){
        			out.print(density.getID() + ".Count"+i1+"to"+i2+"\t");
        		}
        	}
        }
    }

    @Override
    public void log(final long nSample, final PrintStream out) {
    	density = DensityInput.get();
    	if(density.loggable){
	    	density.setSampleMig(true);
	    	density.setAllDemes();
	    	density.setSampleMig(false);
	    	counts=density.getCounts();
    	}else{
	    	counts=null;
	    }
    	int n=density.getNDemes();
    	if (counts!=null){
    		for (int i1=0;i1<n;i1++){
            	for (int i2=0;i2<n;i2++){
            		if (i1!=i2){
            			out.print(counts[i1*n + i2]+"\t");
            		}
            	}
            }
    	}else{
    		for (int i1=0;i1<n;i1++){
            	for (int i2=0;i2<n;i2++){
            		if (i1!=i2){
            			out.print("N\t");
            		}
            	}
            }
    	}
    }
    

    @Override
    public void close(PrintStream out) {
    }

    @Override
    public int getDimension() {
    	density = DensityInput.get();
    	int n=density.getNDemes();
        return n*(n-1);
    }

    @Override
    public double getArrayValue() {
    	//(Arrays.copyOf(lineageCount, lineageCount.length));
    	//return (Arrays.copyOf(DensityInput.get().getCounts(),DensityInput.get().getNDemes()))
        //return getSubTreeLength(treeInput.get().getRoot());
    	return DensityInput.get().getNDemes();
    }

    @Override
    public double getArrayValue(int iDim) {
        //return getSubTreeLength(treeInput.get().getRoot());
    	return DensityInput.get().getNDemes();
    }
    
}
