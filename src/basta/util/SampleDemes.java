package basta.util;


import basta.distributions.StructuredCoalescentTreeDensityVolz;
import basta.evolution.tree.MigrationModelVolz;
import basta.evolution.tree.MultiTypeNodeVolz;
import basta.evolution.tree.MultiTypeTreeVolz;
import beast.base.core.BEASTObject;
import beast.base.core.Input;
import beast.base.evolution.tree.Node;
import beast.base.util.Randomizer;
import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

/**
 * Created by jessiewu on 05/10/16.
 */
public class SampleDemes extends BEASTObject{

    public Input<MigrationModelVolz> migrationModelInput = new Input<MigrationModelVolz>(
            "migrationModelVolz", "Model of migration between demes.",
            Input.Validate.REQUIRED);

    public Input<MultiTypeTreeVolz> mtTreeInput = new Input<MultiTypeTreeVolz>("multiTypeTreeVolz",
            "Volz-type tree.", Input.Validate.REQUIRED);



    public Input<StructuredCoalescentTreeDensityVolz> likInput = new Input<StructuredCoalescentTreeDensityVolz>(
            "bastaLik",
            "An object of StructuredCoalescentTreeDensityVolz class",
            Input.Validate.REQUIRED
    );


    protected MigrationModelVolz migrationModelVolz;
    private int[] migrationCounts;
    protected MultiTypeTreeVolz mtTree;
    protected StructuredCoalescentTreeDensityVolz lik;

    public void initAndValidate(){
        migrationModelVolz = migrationModelInput.get();
        mtTree = mtTreeInput.get();
        lik = likInput.get();
    }

    public void setAllDemes() throws Exception {
        //if (sampleMig){
        migrationModelVolz.updateMatrices();
        migrationCounts = new int[migrationModelVolz.getNDemes()*migrationModelVolz.getNDemes()];
        //}
        setRootDeme();
        MultiTypeNodeVolz root=(MultiTypeNodeVolz) mtTree.getRoot();
        //System.out.println("Root has deme "+root.getNodeType()+"\n");
        //Then make iteration
        if ((root.getLeft()!=null) && (!root.getLeft().isLeaf())){
            Node LC= root.getLeft();
            double LCTime= root.getHeight() - LC.getHeight();
            setDemeIterative(root.getLeft(),LCTime, root.getNodeType());
        }

        if ((root.getRight()!=null) && (!root.getRight().isLeaf())){
            Node RC= root.getRight();
            double RCTime= root.getHeight() - RC.getHeight();
            setDemeIterative(root.getRight(),RCTime, root.getNodeType());
        }
    }

    public void setRootDeme() {
        int type=-1;
        //Double[] probs = lineageProbsList.get(lineageProbsList.size()-1);
        //System.out.println("\n New root\n");
        double x = Randomizer.nextDouble();
        double sum=0.0;
        Double[] rootProbs = new Double[lik.getNDemes()];
        lik.getRootProbs(rootProbs);
        for (int i = 0; i < rootProbs.length; i++) {
            //System.out.println("Probability at index "+i+" = "+probs[i]);
            sum+=rootProbs[i];
            if (x<sum){
                type=i;
                break;
            }
        }
        if (type==-1) {
            System.out.println("Something is wrong with probabilities at the root.");
            for (int i = 0; i< rootProbs.length; i++) {
                System.out.println("Probability at index "+i+" = "+ rootProbs[i]+"\n");
            }
            System.exit(0);
        }
        MultiTypeNodeVolz root=(MultiTypeNodeVolz) mtTree.getRoot();
        root.setNodeType(type);
    }

    /**
     * Set the deme for a node, and then call the function iteratively for the leaves.
     * and conditioned on the deme sampled for their parent.
     */
    public void setDemeIterative(Node node, double time, int parentDeme) throws Exception{
        //System.out.println("New node\n");
        //get probability matrix for the considered branch
        int nTypes = migrationModelVolz.getNDemes();
        double[] pMatrix = new double[nTypes*nTypes];
        if (migrationModelVolz.getJblas()==false){
            //BEAST2 exponentiation
            //System.out.println("Use BEAST exponentiation");
            migrationModelVolz.getTransitionProbabilities(time, pMatrix);
        }else{
            //use jblas to calculate pMatrix, the probability matrix of migration events
            //System.out.println("Use JBLAS");
            DoubleMatrix Qsupp = new DoubleMatrix(nTypes, nTypes);
            for (int i = 0; i < nTypes; i++) {
                Qsupp.put(i,i, 0.0);
                for (int j = 0; j < nTypes; j++) {
                    if (i != j) {
                        Qsupp.put(i, j, migrationModelVolz.getRate(i, j)*time);
                        Qsupp.put(i, i, Qsupp.get(i, i) - Qsupp.get(i, j));
                    }
                }
            }
            DoubleMatrix Fsupp = MatrixFunctions.expm(Qsupp);
            for (int t = 0; t<nTypes; t++) {
                for (int t2 = 0; t2<nTypes; t2++) {
                    pMatrix[(t*nTypes) + t2]=Fsupp.get(t, t2);
                }
            }
        }
        //System.out.println("Calculated p matrix\n");
        //get the lower likelihoods for the node
        //Double[] likes=pLikes[node.getNr()];
        Double[] likes = lik.getLikes(node.getNr());
        //System.out.println("Step 1\n");

        //calculate probabilities for node location
        double[] probs = new double[nTypes];
        double sum=0.0;
        //System.out.println("Step 2\n");
        //System.out.println("likes size "+likes.length+"\n");
        //System.out.println("pMatrix size "+pMatrix.length+"\n");
        for (int t = 0; t<nTypes; t++) {
            //System.out.println("Index t "+t+"\n");
            //System.out.println("pMatrix index "+((t*nTypes) + parentDeme)+"\n");
            probs[t]=likes[t]*pMatrix[(t*nTypes) + parentDeme];
            sum+=probs[t];
        }
        //System.out.println("Step 3\n");
        for (int t = 0; t<nTypes; t++) {
            probs[t]=probs[t]/sum;
        }

        //Set node type!
        //System.out.println("Set node type");
        double x = Randomizer.nextDouble();
        int type=-1;
        sum=0.0;
        for (int i = 0; i<probs.length; i++) {
            sum+=probs[i];
            if (x<sum){
                type=i;
                break;
            }
        }
        if (type==-1) {
            for(int i = 0; i < probs.length; i++){
                System.out.print(probs[i]+" ");
            }
            System.out.println();
            System.out.println("Something is wrong with probabilities.");
            //System.exit(0);
            throw new Exception("Something is wrong with probabilities due to numerical instabilities?");
        }
        if (!node.isLeaf()) {
            ((MultiTypeNodeVolz) node).setNodeType(type);
        }
        //System.out.println("Set node type done");

        //if (sampleMig){
        //System.out.println("Start sampling number of mig");
        //use symmetrized rates?
        boolean sym = false;
        double muL=migrationModelVolz.getMu(sym)*time;
        double Pba= pMatrix[(type*nTypes) + parentDeme];
        //System.out.println("P matrix: "+pMatrix[0]+" "+pMatrix[1]+" "+pMatrix[2]+" "+pMatrix[3]+" ");
        //System.out.println("muL: "+muL+" time: "+time+"  Pba: "+Pba+"  type: "+type+"  parentDeme: "+parentDeme);
        //calculate number of migrations on branch
        int n = drawEventCount(type, parentDeme, muL, Pba, migrationModelVolz, sym);
        //System.out.println("Done counts");
        //sample migration events and add them to the global count
        drawMigrationTypes(n, type, parentDeme, muL, Pba, migrationModelVolz, sym);
        //System.out.println("End sampling number of mig");
        //}
        //System.out.println("Migration type counts\n"+Arrays.toString(migrationCounts));

        //Then make iteration
        if (node.getLeft()!= null){
            Node LC= node.getLeft();
            double LCTime= node.getHeight() - LC.getHeight();
            //System.out.println("launch first iteration\n");
            setDemeIterative(LC ,LCTime, ((MultiTypeNodeVolz) node).getNodeType());
        }

        if (node.getRight()!= null){
            Node RC= node.getRight();
            double RCTime= node.getHeight() - RC.getHeight();
            //System.out.println("launch second iteration\n");
            setDemeIterative(RC ,RCTime, ((MultiTypeNodeVolz) node).getNodeType());
        }

    }

    private int drawEventCount(int typeStart, int typeEnd, double muL, double Pba, MigrationModelVolz migrationModelVolz, boolean sym) {
        int nVirt = 0;
        double u = Randomizer.nextDouble();
        double P_low_given_ab = 0.0;
        double acc = - muL - Math.log(Pba);
        double log_muL = Math.log(muL);

        do {
            P_low_given_ab += Math.exp(Math.log(migrationModelVolz.getRpowN(nVirt, sym).get(typeStart, typeEnd)) + acc);

            if (P_low_given_ab>u)
                return nVirt;
            nVirt += 1;
            acc += log_muL - Math.log(nVirt);
        } while (migrationModelVolz.RpowSteadyN(sym) < 0 || nVirt<migrationModelVolz.RpowSteadyN(sym));

        int thresh = nVirt;


        do {
            nVirt = (int) Randomizer.nextPoisson(muL);

        } while (nVirt < thresh);

        return nVirt;
    }

    protected void drawMigrationTypes(int n, int typeStart, int typeEnd, double muL, double Pba, MigrationModelVolz migrationModelVolz, boolean sym ) {

        // Abort if transition is impossible.
        if (Pba == 0.0)
            return ;

        // Catch for numerical errors
        if (Pba > 1.0 || Pba < 0.0) {
            System.err.println("Warning: matrix exponentiation resulted in rubbish.  Aborting move.");
            System.exit(0);
        }

        // Select number of virtual events:
        int nVirt = n;

        // Sample type changes along branch using FB algorithm:
        int [] subsTypes = new int[migrationModelVolz.getNDemes()*migrationModelVolz.getNDemes()];
        int prevType = typeStart;

        for (int i = 1; i<=nVirt; i++) {
            double u2 = Randomizer.nextDouble()*migrationModelVolz.getRpowN(nVirt-i+1, sym).get(prevType, typeEnd);
            int c;
            boolean fellThrough = true;
            for (c = 0; c<migrationModelVolz.getNDemes(); c++) {
                u2 -= migrationModelVolz.getR(sym).get(prevType,c)*migrationModelVolz.getRpowN(nVirt-i, sym).get(c,typeEnd);
                if (u2<0.0) {
                    fellThrough = false;
                    break;
                }
            }

            // Check for FB algorithm error:
            if (fellThrough) {
                double sum1 = migrationModelVolz.getRpowN(nVirt-i+1, sym).get(prevType, typeEnd);
                double sum2 = 0;
                for (c = 0; c<migrationModelVolz.getNDemes(); c++) {
                    sum2 += migrationModelVolz.getR(sym).get(prevType,c)*migrationModelVolz.getRpowN(nVirt-i, sym).get(c,typeEnd);
                }

                System.err.println("Warning: FB algorithm failure.  Exiting program.");
                System.out.println("sum1: "+sum1+"  sum2: "+sum2+"\n");
                System.exit(0);
                //return Double.NEGATIVE_INFINITY;
            }

            //types[i-1] = c;
            subsTypes[prevType*migrationModelVolz.getNDemes() + c]+=1;
            prevType = c;

        }
        for (int i=0;i<(migrationModelVolz.getNDemes()*migrationModelVolz.getNDemes());i++) migrationCounts[i]+=subsTypes[i];
        //System.out.println("For a branch. S deme: "+typeStart+" end deme: "+typeEnd+" mu*t: "+muL+" substitutions"+subsTypes[0]+" "+subsTypes[1]+" "+subsTypes[2]+" "+subsTypes[3]+" "+"\n");
    }

    public void getMigrationCounts(int[] counts){
        System.arraycopy(migrationCounts, 0, counts, 0, migrationCounts.length);
    }
}