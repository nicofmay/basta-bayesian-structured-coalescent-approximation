/*
 * Copyright (C) 2014 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package basta.util;

import basta.distributions.StructuredCoalescentTreeDensityVolz;
import beast.base.core.Description;
import beast.base.core.Function;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.core.Loggable;
import beast.base.inference.CalculationNode;

import java.io.PrintStream;

@Description("Logger to report structured coalescent tree prior.")
public class TreePriorLoggerVolz extends CalculationNode implements Loggable, Function {

    public Input<StructuredCoalescentTreeDensityVolz> multiTypeTreeInput = new Input<StructuredCoalescentTreeDensityVolz>(
            "structuredCoalescentTreeDensityVolz", "StructuredCoalescentTreeDensityVolz to report root type.", Validate.REQUIRED);
    
    StructuredCoalescentTreeDensityVolz sCoal;
    //MultiTypeTreeVolz mtTree;
    
    @Override
    public void initAndValidate() {
        //mtTree = multiTypeTreeInput.get();
        sCoal = multiTypeTreeInput.get();
    }

    @Override
    public void init(PrintStream out) throws IllegalArgumentException {
        if (getID() == null || getID().matches("\\s*")) {
            out.print(sCoal.getID() + ".treePrior\t");
        } else {
            out.print(getID() + "\t");
        }
    }

    @Override
    public void log(long nSample, PrintStream out) {
    	out.print(sCoal.getLogP() + "\t");
    }

    @Override
    public void close(PrintStream out) { };

    @Override
    public int getDimension() {
        return 1;
    }

    @Override
    public double getArrayValue() {
    	return sCoal.getLogP();
    }

    @Override
    public double getArrayValue(int iDim) {
    	return sCoal.getLogP();
    }
}

